

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include <iostream>
#include "math.h"
#include <stdio.h>
#include <float.h>
#include "imageloader.h" // For rendering the bmp files
#include <cstdlib>
#include <ctime>

using namespace std;

GLfloat i=1;


GLfloat xrotation=0;

GLfloat yrotation=0;
GLfloat zrotation=0;


GLuint textureId[6];
int facevalue=2;

#define PI 3.141592655


void generatediceface()
{
	srand(time(NULL));
	facevalue = (rand() % 6) + 1;
}

GLuint loadTexture2(Image* image) {
	/** loadTexture2: Loads the texture for terrain in memory */
	
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) ;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0,
			GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return textureId;
}


void dicetexture()
{
	Image* image1 = loadBMP("./images/red_1.bmp");
	textureId[0] = loadTexture2(image1);
	delete image1;

	

	Image* image2 = loadBMP("./images/red_2.bmp");
	textureId[1] = loadTexture2(image2);
	delete image2;

	
	Image* image3 = loadBMP("./images/red_3.bmp");
	textureId[2] = loadTexture2(image3);
	delete image3;

	Image* image4 = loadBMP("./images/red_4.bmp");
	textureId[3] = loadTexture2(image4);
	delete image4;

	Image* image5 = loadBMP("./images/red_5.bmp");
	textureId[4] = loadTexture2(image5);
	delete image5;

	Image* image6 = loadBMP("./images/red_6.bmp");
	textureId[5] = loadTexture2(image6);
	delete image6;

	
	
	
}


void displaydice()
{
	GLfloat sizeofdice=0.3;					// Size of the Cube
	glClear(GL_COLOR_BUFFER_BIT);			// Clearing thE Color Buffer
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glMatrixMode(GL_MODELVIEW);				// Modifying the ModelView Matrix
	glLoadIdentity();						// Setting the Matrix to Identity.
	glRotatef(xrotation,0.5,0.0,0.0);		// Rotation about x-axis
	glRotatef(yrotation,0.0,0.5,0.0);       // Rotation about y-axis
	glRotatef(zrotation,0.0,0.0,0.5);		// Rotation about z-axis
	//glRotatef(90,0.5,0.0,0.0);	
	glEnable( GL_CULL_FACE );
    glCullFace ( GL_BACK );
	glEnable(GL_TEXTURE_2D);
	

	// All the Quadrilaterals are made in counter-clockwise fashion.

	// Front Face with "1"
	glBindTexture(GL_TEXTURE_2D,textureId[0]);
	glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice); 
   	glEnd();


   
   // Back Face with "6"
	glBindTexture(GL_TEXTURE_2D,textureId[5]);
   	glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);
    glEnd();


    // Top Face with "2"
    glBindTexture(GL_TEXTURE_2D,textureId[1]);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice);  
    glEnd();


	// Bottom Face with "5"
    glBindTexture(GL_TEXTURE_2D,textureId[4]);
    glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice); 
    glEnd();



    // Right face with "3"
    glBindTexture(GL_TEXTURE_2D,textureId[2]);
    glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice); 
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice); 
    glEnd();


    // Left Face with "4"
    glBindTexture(GL_TEXTURE_2D,textureId[3]);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice); 
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice); 
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice); 
	glEnd();


	glutSwapBuffers();

	

	if(facevalue==1)
	{
		if(yrotation<360.00)
		{
			xrotation=xrotation+0.03;
			yrotation=yrotation+0.03;
			zrotation=zrotation+0.03;
			//cout << i << endl;
			glutPostRedisplay();
			//generatediceface();
		}
	}

	if(facevalue==2)
	{
		
		if(xrotation<90.00)
		{
			xrotation=xrotation+0.03;
			yrotation=yrotation+0.12;
			zrotation=zrotation+0.12;
			//cout << i << endl;
			glutPostRedisplay();
			//generatediceface();
		}
	}
	

	if(facevalue==6)
	{
		
		if(xrotation<180.00)
		{
			xrotation=xrotation+0.03;
			yrotation=yrotation+0.06;
			zrotation=zrotation+0.06;
			//cout << i << endl;
			glutPostRedisplay();
			//generatediceface();
		}
	}
	


	
	
	
	
	


}

void initial()
{
	
	glClearColor(0.0,0.0,0.0,0.0);
	glShadeModel(GL_FLAT);
	
}


int main(int argc,char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE| GLUT_RGB);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(10,10);
	glutCreateWindow("Dice Rolling");

	dicetexture();
	initial();
	
	
	glutDisplayFunc(displaydice);
	//glutReshapeFunc(handleResize);
	glutMainLoop();
	return 0;
}
