%{
void yyerror(char *s);
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

%}

%token CURRENCY STARTINGMONEY JAILFINE TAX 

%union{
	std::string *str;
	int num;
}
%token <number> INTEGER
%token <str> WORD

%%
statements:
		| statement statements
		;
statement:
		scurrency
		|
		sstartingmoney
		|
		sjailfine
		|
		stax
		|
		slocation
		|
		sroute
		|
		scost
		|
		srent
		;
scurrency:CURRENCY WORD
			{
				printf("Currency is %s\n",$2);
			}
sstartingmoney:STARTINGMONEY INTEGER
			{
				printf("Each Player starts with %d\n",$2);
			}
sjailfine:JAILFINE INTEGER
			{
				printf("Jailfine is %d\n",$2);
			}
stax:
TAX INTEGER "%" INTEGER
{
	printf("Tax is set to either %d or is %d percent \n",$2,$4 );
}

%%

int yywrap()
{
	return 1;
}

extern FILE * yyin;

int finalparser()
{
	yyin=fopen("config.txt","r");
	yyparse();
	return 1;
}

