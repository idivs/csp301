%{
#include <iostream>
#include <stdio.h>
#include <string>
#include "scanner.tab.h"
using namespace std;
%}
%%
"Tax" 									return TAX;
"Currency" 								return CURRENCY;
"StartingMoney" 						return STARTINGMONEY;
"JailFine" 								return JAILFINE;
"location"								return LOCATION;
"route"									return ROUTE;
"cost"									return COST;
"rent"									return RENT;
"%" 									return PERCENT;
"#"[a-zA-Z0-9.-_=% ]+ 					;
"@l" 									return LOCATIONTOKEN;
"@g"									return GROUPTOKEN;
[0-9]+ 									yylval.num=atoi(yytext);return INTEGER;
[a-zA-Z_]+ 								yylval.str=new string(yytext); return WORD;

%%
int yywrap(void)
{
	return 1;
}