#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "imageloader.h" // For rendering the bmp files
#include <cstdlib>
#include <ctime>

using namespace std;

#define PI 3.14159265
GLuint mainwindow,gamewindow,dicewindow;

void handleKeypress(unsigned char key, int x, int y) 
{
  switch (key) 
  {
    case 27: //Escape key
    exit(0);
  }
}
// Picking Stuff //
#define RENDER					1
#define SELECT					2
#define BUFSIZE 1024
GLuint selectBuf[BUFSIZE];
GLint hits;
int mode = RENDER;
int cursorX,cursorY;
GLuint selectedsnowman=0;

float vertices123[20][3];

void generaterandompoints()
{
  for(int i=0;i<20;i++)
  {

   float theta = 2*PI * (rand()%1000)/1000.0;//random range is 0.0 to 1.0
   float phi = acos(2.0f * (rand()%1000)/1000.0 - 1.0f)*180/(PI);
   float rho = 3;
    float y=rho*cos(phi*PI/180);
    float z=-(rho*sin(phi*PI/180)*cos(theta));
    float x=rho*sin(phi*PI/180)*sin(theta);
  	vertices123[i][0]=x;
  	vertices123[i][1]=y;
  	vertices123[i][2]=z;
  	cout << phi << endl;
  	//cout << x << " " << y << " " << z << endl;
  	//cout << (rand()%1000)/1000.0 << endl;
  	//cout << rho << " " << theta << " " << phi << endl;
  }
}

//Makes the image into a texture, and returns the id of the texture
GLuint loadTexture(Image* image) {


  GLuint textureId;


  glGenTextures(1, &textureId); //Make room for our texture


  glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit


  //Map the image to the texture


  glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D


         0,                            //0 for now


         GL_RGB,                       //Format OpenGL uses for image


         image->width, image->height,  //Width and height


         0,                            //The border of the image


         GL_RGB, //GL_RGB, because pixels are stored in RGB format


         GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored


                           //as unsigned numbers

         image->pixels);               //The actual pixel data
  return textureId; //Returns the id of the texture
}
GLuint _textureId; //The id of the textur
GLUquadric *quad;
float rotate;
//GLUquadricObj quad;

void initRendering() 
{
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_LIGHTING);
  //glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  quad = gluNewQuadric();
  Image* image = loadBMP("worldmap.bmp");
  _textureId = loadTexture(image);
  delete image;
}

void processHits2 (GLint hits, GLuint buffer[], int sw)
{
   GLint i, j, numberOfNames;
   GLuint names, *ptr, minZ,*ptrNames;

   ptr = (GLuint *) buffer;
   minZ = 0xffffffff;
   for (i = 0; i < hits; i++) {	
      names = *ptr;
	  ptr++;
	  if (*ptr < minZ) {
		  numberOfNames = names;
		  minZ = *ptr;
		  ptrNames = ptr+2;
	  }
	  
	  ptr += names+2;
	}
   if (numberOfNames > 0) {
    
	  printf ("You picked snowman  ");
	  ptr = ptrNames;
	  for (j = 0; j < numberOfNames; j++,ptr++) {
      selectedsnowman=*ptr;
      //cout << selectedsnowman;
		 printf ("%d ", *ptr);
	  }
	}
   else
	   printf("You didn't click a snowman!");
  printf ("\n");
   
}
void startPicking() 
{
	GLint viewport[4];
	float ratio;
	glSelectBuffer(BUFSIZE,selectBuf);
	glGetIntegerv(GL_VIEWPORT,viewport);
	glRenderMode(GL_SELECT);
	glInitNames();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPickMatrix(cursorX,viewport[3]-cursorY,5,5,viewport);
	ratio = (viewport[2]+0.0) / viewport[3];
	gluPerspective(45,ratio,0.1,1000);
	glMatrixMode(GL_MODELVIEW);
}
void stopPicking() {

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glFlush();
	hits = glRenderMode(GL_RENDER);
	if (hits != 0){
		processHits2(hits,selectBuf,0);

	}
	mode = RENDER;
}

void handleResize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

}

void drawScene() 
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);  
  glLoadIdentity();
  glTranslatef(0.0f, 1.0f, -16.0f);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, _textureId);
  //Bottom
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


  //glRotatef(rotate,1.0f,0.0f,0.0f);


  //glRotatef(rotate,0.0f,1.0f,0.0f);
//glRotatef(rotate,0.0f,0.0f,1.0f);


    gluQuadricTexture(quad,1);
    gluSphere(quad,3,20,20);
	
	if (mode == SELECT) {
		startPicking();
	}
	//glLoadIdentity();


	for(int kj=0;kj<20;kj++)
	{
		glPushMatrix();
		glPushName(kj);
		glTranslatef(vertices123[kj][0],vertices123[kj][1],vertices123[kj][2]);
		//glColor3f(kj*1.0f/12,kj*1.0f/12,kj*1.0f/12);
		gluSphere(quad,0.3,20,20);
		//glutSolidSphere(0.3,20,20);
		glPopName();
		glPopMatrix();

	}

	//cout << theta << " " << phi << " " << rho << endl;

  if (mode == SELECT)
		stopPicking();
	else
		glutSwapBuffers();

}

void mouseStuff(int button, int state, int x, int y) 
{
	if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN)
		return;
	cursorX = x;
	cursorY = y;
	mode = SELECT;
}

void processMousePassiveMotion(int x, int y) 
{
  cursorX = x;
  cursorY = y;
  mode = SELECT;
}

void update(int value)
{
    rotate+=1.0f;
    if(rotate>360.f)
    {
        rotate-=360;
    }
    glutPostRedisplay();
    glutTimerFunc(25,update,0);
}


void reshape_dice(int width,int height)
{
  glMatrixMode(GL_PROJECTION);                      // Select The Projection Matrix
  glViewport(0,0,width,height);                     // Reset The Current Viewport
  glLoadIdentity();
  gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);     // Calculate The Aspect Ratio Of The Window
  //gluOrtho2D(0, width, height, 0);
  glMatrixMode(GL_MODELVIEW); 
  glLoadIdentity();
}

void displaysdice()
{ 
  
  glutSetWindow(dicewindow);
  glClearColor(0.9, 0.8, 0.8, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0,0,-10);
    if(selectedsnowman!=0)
    {
      glutSolidSphere(1,20,20);
    }

    glutPostRedisplay();
    glutSwapBuffers();
}

int main(int argc, char** argv) {	
 
srand (time(NULL));
generaterandompoints();

  glutInit(&argc, argv);


  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);


  glutInitWindowSize(800, 800);
  mainwindow = glutCreateWindow("Monopoly");
  

  gamewindow = glutCreateSubWindow(mainwindow,0,0,600,800);
  initRendering();
  glutTimerFunc(25,update,0);
  glutDisplayFunc(drawScene);
  glutKeyboardFunc(handleKeypress);
  glutReshapeFunc(handleResize);
  glutMouseFunc(mouseStuff);
  //glutPassiveMotionFunc(processMousePassiveMotion);

  dicewindow = glutCreateSubWindow(mainwindow,600,0,200,800);
  glutReshapeFunc(reshape_dice);
  glutDisplayFunc(displaysdice);
  glutMainLoop();
  return 0;


}
