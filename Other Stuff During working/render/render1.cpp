#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

using namespace std;


std::vector< GLfloat > vertices1,uv1,normal1;
std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;


void displaydice()
{
	

	FILE * file = fopen("ObjModels/City2/City1.obj", "r");
	if( file == NULL )
	{
    	printf("Impossible to open the file !\n");
    	
	}
	while( 1 )
	{
	    char lineHeader[128];
	    int res = fscanf(file, "%s", lineHeader);
	    if (res == EOF)
	    {
	     	break; // EOF = End Of File. Quit the loop.
	    } 
	    GLfloat tempvert123[3];
	    GLfloat tempuv123[2];
	    GLfloat tempnormals123[3];

	    if ( strcmp( lineHeader, "v" ) == 0 )
	    {	
	    	
		    fscanf(file, "%f %f %f\n", &tempvert123[0],&tempvert123[1],&tempvert123[2] );
		    vertices1.push_back(tempvert123[0]);
		    vertices1.push_back(tempvert123[1]);
		    vertices1.push_back(tempvert123[2]); 
		    
	    }
	    else if ( strcmp( lineHeader, "vt" ) == 0 )
	    {
		    fscanf(file, "%f %f\n", &tempuv123[0], &tempuv123[1]);
		    uv1.push_back(tempuv123[0]);
		    uv1.push_back(tempuv123[1]);
		}
		else if ( strcmp( lineHeader, "vn" ) == 0 )
		{
		    fscanf(file, "%f %f %f\n", &tempnormals123[0], &tempnormals123[1], &tempnormals123[2]);
		    normal1.push_back(tempnormals123[0]);
		    normal1.push_back(tempnormals123[1]);
		    normal1.push_back(tempnormals123[2]);  
		}

		else if ( strcmp( lineHeader, "f" ) == 0 )
		{
		    //std::string vertex1, vertex2, vertex3;
		    unsigned int vertexIndex[4], uvIndex[4], normalIndex[4];
		    //int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2],&vertexIndex[3], &uvIndex[3], &normalIndex[3]);
		   	int matches = fscanf(file, "%d/%d %d/%d %d/%d %d/%d\n", &vertexIndex[0], &uvIndex[0], &vertexIndex[1], &uvIndex[1],&vertexIndex[2], &uvIndex[2],&vertexIndex[3], &uvIndex[3]);

		    if (matches !=8)
		    {
		        printf("File can't be read by our simple parser : ( Try exporting with other options\n");
		        
			}

			
			
			vertexIndex[0]=vertexIndex[0]-1;
			vertexIndex[1]=vertexIndex[1]-1;
			vertexIndex[2]=vertexIndex[2]-1;
			vertexIndex[3]=vertexIndex[3]-1;
			uvIndex[0]=uvIndex[0]-1;
			uvIndex[1]=uvIndex[1]-1;
			uvIndex[2]=uvIndex[2]-1;
			uvIndex[3]=uvIndex[3]-1;
			

		    vertexIndices.push_back(vertexIndex[0]);
		    vertexIndices.push_back(vertexIndex[1]);
		    vertexIndices.push_back(vertexIndex[2]);
		    vertexIndices.push_back(vertexIndex[3]);

			uvIndices.push_back(uvIndex[0]);
		    uvIndices.push_back(uvIndex[1]);
		    uvIndices.push_back(uvIndex[2]);
		    uvIndices.push_back(uvIndex[3]);
	

		    /*
		    uvIndices.push_back(uv1[uvIndex[0]]);
		    uvIndices.push_back(uv1[uvIndex[1]]);
		    uvIndices.push_back(uv1[uvIndex[2]]);
		    uvIndices.push_back(uv1[uvIndex[3]]);
		    
		    
		    uvIndices    .push_back(uvIndex[1]);
		    uvIndices    .push_back(uvIndex[2]);
		    uvIndices    .push_back(uvIndex[3]);
		    
			*/
		    
		    normalIndices.push_back(normalIndex[0]-1);
		    normalIndices.push_back(normalIndex[1]-1);
		    normalIndices.push_back(normalIndex[2]-1);
		    normalIndices.push_back(normalIndex[3]-1);
		    
		}
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
	glLoadIdentity();					// Reset The Current Modelview Matrix
    glTranslatef(-1.0f,0.0f,-0.0f);				// Move Right 1.5 Units And Into The Screen 6.0

    /*
	glBegin(GL_QUADS);
	for (unsigned int i=0; i<vertexIndices.size(); i++)
	{
		glVertex3f(vertices1[(vertexIndices[i]-1)*3], vertices1[(vertexIndices[i]-1)*3+1], vertices1[(vertexIndices[i]-1)*3+2]);
		//glVertex3f(vertices1[(vertexIndices[i+1]-1)*3], vertices1[(vertexIndices[i+1]-1)*3+1], vertices1[(vertexIndices[i+1]-1)*3+2]);
		//glVertex3f(vertices1[(vertexIndices[i+2]-1)*3], vertices1[(vertexIndices[i+2]-1)*3+1], vertices1[(vertexIndices[i+2]-1)*3+2]);
		//glVertex3f(vertices1[(vertexIndices[i+3]-1)*3], vertices1[(vertexIndices[i+3]-1)*3+1], vertices1[(vertexIndices[i+3]-1)*3+2]);

		glTexCoord2f(uv1[(uvIndices[i]-1)*2], uv1[(uvIndices[i]-1)*2+1]);
		//glNormal3f(normals.at(faces.at(i).normals[0]-1)->x, normals.at(faces.at(i).normals[0]-1)->y, normals.at(faces.at(i).normals[0]-1)->z);
		
	}
	glEnd();
	*/

    
    glEnableClientState(GL_VERTEX_ARRAY);
    //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    //glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &vertices1[0]);
    glColorPointer(4, GL_FLOAT, 0, &normal1[0]);
    //glNormalPointer(GL_FLOAT, 0, &normal1[0]);
    //glTexCoordPointer(2, GL_FLOAT, 0, &uv1[0]);
    glDrawElements(GL_QUADS, vertexIndices.size(), GL_UNSIGNED_INT, &vertexIndices[0]);
    //glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
    //glDisableClientState(GL_COLOR_ARRAY);
    //glDisableClientState(GL_NORMAL_ARRAY);
    
	glutSwapBuffers();
	
}

void initial()
{
	glClearColor(0.0,0.0,0.0,0.0);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glEnable ( GL_COLOR_MATERIAL );
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void reshape (int width , int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();  
}

int main(int argc,char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(10,10);
	glutCreateWindow("Render");
	initial();
	glutDisplayFunc(displaydice);
	glutReshapeFunc(reshape);
	glutMainLoop();
	return 0;
	
}
