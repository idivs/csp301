%{
	#include "monoheader.h"
%}
%%
"Currency"				return CURRENCY;
"StartingMoney"			return STARTINGMONEY;
"JailFine"				return JAILFINE;
"Tax"					return TAX;
"location"			return LOCATION;
"route"					return ROUTE;
"cost"					return COST;
"rent"					return RENT;
#						return HASH;
\n						return NEWLINE;
[@a-zA-Z][_a-zA-Z0-9]* 	        return IDENTIFIER;
[0-9][0-9]* 			return INTEGER;
[ \t:=]					;
. 						;
%%
int yywrap(void)
{
	return 1;
}
