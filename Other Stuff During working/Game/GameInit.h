/*
	GameInit.cpp 
	Created by :
	Divyanshu Rathi
	5/09/2014
*/

#ifndef __GAMEINIT_H_INCLUDED__  
#define __GAMEINIT_H_INCLUDED__

#include <iostream>
#include <string>
#include <vector>
#include "math.h"
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
using namespace std;


extern bool isturncompleted[5];
extern int whichplayerturn;
extern int startlocationno;
extern int kplane;
extern float jplane;
extern bool showplanetransition;
extern int playerwhocanbuy;

class City
{
public:
	string* name;
	int group;
	int cost;
	int mortagecost;
	int house1cost;
	int house2cost;
	int house3cost;
	int house4cost;
	int hotelcost;
	int rent0;
	int rent1;
	int rent2;
	int rent3;
	int rent4;
	int renthotel;
	int isbought;
	int boughtby;
	int totalhouses;
	int ishotel;
};

class Game
{
public:
	string* currency;
	int startingmoney;
	int jailfine;
	int tax;
	vector <City> cities;
	int map[50][50];
	int totalcolorgroups;
	float colorcodes[20][3];
	void Generatecolorgroups();
};



#endif