#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"
#include "dice.h"

int facevalue=5;
int diceflag=1;
bool isdicecomplete=0;
GLfloat xrotation=1;
GLfloat yrotation=1;
GLfloat zrotation=1;
GLuint textureIddice[6];
bool isdicerolled=0;
int remainingvalueofdice=0;

void dicetexture(void)
{
	Image* image1 = loadBMP("./images/red_1.bmp");
	textureIddice[0] = loadTexture2(image1);
	delete image1;

	Image* image2 = loadBMP("./images/red_2.bmp");
	textureIddice[1] = loadTexture2(image2);
	delete image2;

	
	Image* image3 = loadBMP("./images/red_3.bmp");
	textureIddice[2] = loadTexture2(image3);
	delete image3;

	Image* image4 = loadBMP("./images/red_4.bmp");
	textureIddice[3] = loadTexture2(image4);
	delete image4;

	Image* image5 = loadBMP("./images/red_5.bmp");
	textureIddice[4] = loadTexture2(image5);
	delete image5;

	Image* image6 = loadBMP("./images/red_6.bmp");
	textureIddice[5] = loadTexture2(image6);
	delete image6;

}


void renderdice()
{
	GLfloat sizeofdice=0.6;					// Size of the Cube
	glEnable(GL_TEXTURE_2D);

	// All the Quadrilaterals are made in counter-clockwise fashion.

	// Front Face with "1"
	glBindTexture(GL_TEXTURE_2D,textureIddice[0]);
	glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice); 
   	glEnd();

   // Back Face with "6"
	glBindTexture(GL_TEXTURE_2D,textureIddice[5]);
   	glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);
    glEnd();


    // Top Face with "2"
    glBindTexture(GL_TEXTURE_2D,textureIddice[1]);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice);  
    glEnd();


	// Bottom Face with "5"
    glBindTexture(GL_TEXTURE_2D,textureIddice[4]);
    glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice); 
    glEnd();



    // Right face with "3"
    glBindTexture(GL_TEXTURE_2D,textureIddice[2]);
    glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice, -sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice, -sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice); 
    glTexCoord2f(0.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice); 
    glEnd();


    // Left Face with "4"
    glBindTexture(GL_TEXTURE_2D,textureIddice[3]);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice, -sizeofdice); 
    glTexCoord2f(1.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice); 
    glTexCoord2f(1.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice); 
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice, -sizeofdice); 
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void setdicevalue()
{
	if(facevalue==1)
	{
		diceflag=1;
		if(yrotation<360.00)
		{
			xrotation=xrotation+6;
			yrotation=yrotation+6;
			zrotation=zrotation+6;
			//cout << i << endl;
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}
	}


	if(facevalue==2)
	{
		diceflag=1;
		if(xrotation<90.00)
		{
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;
			//cout << xrotation << endl;
			//cout << i << endl;	
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}
	}

	if(facevalue==3)
	{
		diceflag=1;
		if(yrotation<270)
		{
			xrotation=xrotation+8;
			yrotation=yrotation+2;
			zrotation=zrotation+8;	
		}
		else
		{
			isdicecomplete=1;
		}
	}
	if(facevalue==4)
	{
		diceflag=1;
		if(yrotation<90)
		{
			xrotation=xrotation+8;
			yrotation=yrotation+2;
			zrotation=zrotation+8;	
		}
		else
		{
			isdicecomplete=1;
		}
	}
	if(facevalue==5)
	{
		diceflag=1;
		if(xrotation<270)
		{
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;	
		}
		else
		{
			isdicecomplete=1;
		}
	}
	
	if(facevalue==6)
	{
		diceflag=1;
		if(xrotation<180.00)
		{	
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;
			//cout << xrotation << " " << yrotation << endl;
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}		
	}
	if(isdicecomplete==1)
	{
		/*Font with the text will be displayed here */
			//cout << "The value is : " << facevalue;
	}
}
