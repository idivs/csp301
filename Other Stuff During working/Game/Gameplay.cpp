#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"
#include "Player.h"
#include "Gameplay.h"
GLMmodel *citymodel;

extern Game Monopoly;
extern Player p1;
int locationbuytag=0;
int userwantstobuy=0;
void loadcityingame()
{
	glmFacetNormals(citymodel);
	glmVertexNormals(citymodel, 1000);
	glmDraw(citymodel,GLM_MATERIAL |  GLM_SMOOTH);
}

void buylocation()
{
	
	if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought!=1)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].cost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].cost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby=whichplayerturn;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][0]=1;
			//cout << "HI there : " << p1.citiesowned[p1.currentlocation[whichplayerturn]][0] << endl;
			p1.noofcitiesowned[whichplayerturn]=p1.noofcitiesowned[whichplayerturn]+1;
			p1.citiesofeachgroup[whichplayerturn][Monopoly.cities[p1.currentlocation[whichplayerturn]].group]=p1.citiesofeachgroup[whichplayerturn][Monopoly.cities[p1.currentlocation[whichplayerturn]].group]+1;
		}
	}
	else if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought==1 && whichplayerturn==Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby && Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses==0)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].house1cost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].house1cost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][1]=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses=Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought==1 && whichplayerturn==Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby && Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses==1)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].house2cost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].house2cost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][2]=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses=Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought==1 && whichplayerturn==Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby && Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses==2)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].house3cost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].house3cost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][3]=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses=Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought==1 && whichplayerturn==Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby && Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses==3)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].house4cost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].house4cost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][4]=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses=Monopoly.cities[p1.currentlocation[whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[p1.currentlocation[whichplayerturn]].isbought==1 && whichplayerturn==Monopoly.cities[p1.currentlocation[whichplayerturn]].boughtby && Monopoly.cities[p1.currentlocation[whichplayerturn]].ishotel==0)
	{
		if(p1.hismoney[whichplayerturn]>=Monopoly.cities[p1.currentlocation[whichplayerturn]].hotelcost)
		{
			p1.hismoney[whichplayerturn]=p1.hismoney[whichplayerturn]-Monopoly.cities[p1.currentlocation[whichplayerturn]].hotelcost;
			cout << "His money is : " << p1.hismoney[whichplayerturn] << endl;
			p1.citiesowned[whichplayerturn][p1.currentlocation[whichplayerturn]][5]=1;
			Monopoly.cities[p1.currentlocation[whichplayerturn]].ishotel=1;
		}
	}


}