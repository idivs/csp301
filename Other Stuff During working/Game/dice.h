/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef DICE_H_INCLUDED
#define DICE_H_INCLUDED

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"

extern int facevalue;;
extern int diceflag;
extern bool isdicecomplete;
extern GLfloat xrotation;
extern GLfloat yrotation;
extern GLfloat zrotation;
extern bool isdicerolled;
extern int remainingvalueofdice;

void setdicevalue();
void renderdice(void);
void dicetexture(void);

#endif