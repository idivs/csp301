#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"
#include "Player.h"
extern Game Monopoly;

Player p1;
GLuint DLplayer;
void renderplayercard(int i)
{		
		//cout << "checking how many times it is " << endl;
		char properycost[30];
		sprintf(properycost,"%d",Monopoly.cities[i].cost);
		char costwithh1[10];
		sprintf(costwithh1,"%d",Monopoly.cities[i].house1cost);
		char costwithh2[10];
		sprintf(costwithh2,"%d",Monopoly.cities[i].house2cost);
		char costwithh3[10];
		sprintf(costwithh3,"%d",Monopoly.cities[i].house3cost);
		char costwithh4[10];
		sprintf(costwithh4,"%d",Monopoly.cities[i].house4cost);
		char costwithhotel[10];
		sprintf(costwithhotel,"%d",Monopoly.cities[i].hotelcost);
		char mortage[10];
		sprintf(mortage,"%d",Monopoly.cities[i].mortagecost);
		char rent0[10];
		sprintf(rent0,"%d",Monopoly.cities[i].rent0);
		char rent1[10];
		sprintf(rent1,"%d",Monopoly.cities[i].rent1);
		char rent2[10];
		sprintf(rent2,"%d",Monopoly.cities[i].rent2);
		char rent3[10];
		sprintf(rent3,"%d",Monopoly.cities[i].rent3);
		char rent4[10];
		sprintf(rent4,"%d",Monopoly.cities[i].rent4);
		char renthotel[10];
		sprintf(renthotel,"%d",Monopoly.cities[i].renthotel);

		//glEnable(GL_TEXTURE_2D);
		//glBindTexture(GL_TEXTURE_2D,textureId[0]);

		glColor3f(Monopoly.colorcodes[Monopoly.cities[i].group-1][0],Monopoly.colorcodes[Monopoly.cities[i].group-1][1],Monopoly.colorcodes[Monopoly.cities[i].group-1][2]);
		glBegin(GL_QUADS);
	    glTexCoord2f(0.0f, 0.0f); glVertex3f(-breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 0.0f); glVertex3f( breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 1.0f); glVertex3f( breadthofcard,  lengthofcard,  -10);  
	    glTexCoord2f(0.0f, 1.0f); glVertex3f(-breadthofcard,  lengthofcard,  -10); 
	   	glEnd();
	   	//glDisable(GL_TEXTURE_2D);

	   	glColor3f(1,1,1);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.999);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.999); 
	    
	   	glEnd();
	   	glColor3f(0.984,0.8352,0.611);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.990);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.990); 
	   	glEnd();

	   	glColor3f(0.0,0.0,0.0);
	   	
	   	renderBitmapString(-0.7f, 0.8f, -9.989f, (void *)font1 ,(char*)Monopoly.cities[i].name->c_str());

	   	renderBitmapString(-0.7f, 0.55f, -9.989f, (void *)font ,ccost);
	   	renderBitmapString(0.4f, 0.55f, -9.989f, (void *)font ,properycost);

	   	renderBitmapString(-0.7f, 0.35f, -9.989f, (void *)font ,ch1);
	   	renderBitmapString(0.4f, 0.35f, -9.989f, (void *)font ,costwithh1);

	   	renderBitmapString(-0.7f, 0.15f, -9.989f, (void *)font ,ch2);
	   	renderBitmapString(0.4f, 0.15f, -9.989f, (void *)font ,costwithh2);

	   	renderBitmapString(-0.7f, -0.05f, -9.989f, (void *)font ,ch3);
	   	renderBitmapString(0.4f, -0.05f, -9.989f, (void *)font ,costwithh3);

	   	renderBitmapString(-0.7f, -0.25f, -9.989f, (void *)font ,ch4);
	   	renderBitmapString(0.4f, -0.25f, -9.989f, (void *)font ,costwithh4);

	   	renderBitmapString(-0.7f, -0.45f, -9.989f, (void *)font ,chotel);
	   	renderBitmapString(0.4f, -0.45f, -9.989f, (void *)font ,costwithhotel);

	   	renderBitmapString(-0.7f, -0.65f, -9.989f, (void *)font ,constantrent);
	   	renderBitmapString(0.4f, -0.65f, -9.989f, (void *)font ,rent0);

	 
	   	renderBitmapString(-0.7f, -0.85f, -9.989f, (void *)font ,crent1);
	   	renderBitmapString(0.4f, -0.85f, -9.989f, (void *)font ,rent1);

	   	renderBitmapString(-0.7f, -1.05f, -9.989f, (void *)font ,crent2);
	   	renderBitmapString(0.4f, -1.05f, -9.989f, (void *)font ,rent2);

	   	renderBitmapString(-0.7f, -1.25f, -9.989f, (void *)font ,crent3);
	   	renderBitmapString(0.4f, -1.25f, -9.989f, (void *)font ,rent3);

	   	renderBitmapString(-0.7f, -1.45f, -9.989f, (void *)font ,crent4);
	   	renderBitmapString(0.4f, -1.45f, -9.989f, (void *)font ,rent4);

	   	renderBitmapString(-0.7f, -1.65f, -9.989f, (void *)font ,chostel1);
	   	renderBitmapString(0.4f, -1.65f, -9.989f, (void *)font ,renthotel);

	   	renderBitmapString(-0.7f, -1.85f, -9.989f, (void *)font ,cmort);
	   	renderBitmapString(0.4f, -1.85f, -9.989f, (void *)font ,mortage);

	   	//renderBitmapString(-0.9f, 0.5f, -9.989f, (void *)font1 ,rent1);
	   	//renderBitmapString(-0.7f, 0.8f, -9.989f, (void *)font1 ,propertyname);
}

void initialiseplayer()
{
	for(int i=0;i<5;i++)
	{
		p1.hismoney[i]=Monopoly.startingmoney;
		p1.noofcitiesowned[i]=0;
		p1.currentlocation[i]=0;
		p1.temporarylocation[i]=0;
	}
	
	for (int i = 0; i < Monopoly.cities.size(); ++i)
	{
		for(int j=0;j<5;j++)
		{
			p1.citiesowned[j][i][0]=0;
			p1.citiesowned[j][i][1]=0;
			p1.citiesowned[j][i][2]=0;
			p1.citiesowned[j][i][3]=0;
		}
	}
	for (int i = 1; i < Monopoly.totalcolorgroups; ++i)
	{
	    for(int j=0;j<5;j++)
	    {
			p1.citiesofeachgroup[j][i]=0;
		}
	}

}
/*
GLuint createplayerDL() {
	GLuint cardDL,loopDL;

	cardDL = glGenLists(1);
	loopDL = glGenLists(1);

	//char *propertyname = new char[Monopoly.cities[i].name->length() + 1];
	int ijkl=1;
	float horshifting=4.1;
	float vershifting=2.7;
	int noofcards=Monopoly.cities.size()-1;
	//int noofcards=28;
	//char propertyname[50]="Meditarrian Avenue";


	//cout << noofcards << endl;

	
	glNewList(loopDL,GL_COMPILE);
	while(ijkl<=noofcards)
	{
		if(p1.citiesowned[ijkl][0]==1)
		{
			glLoadIdentity();
			glTranslatef(-horshifting,vershifting,0);
			glColor3f(1.0,1.0,1.0);
			//glCallList(cardDL);
			//renderBitmapString(-0.7f, 0.55f, -9.989f, (void *)font ,ccost);
			//renderplayercard(ijkl);
			
			horshifting=horshifting-2.1;
			if(ijkl%5==0)
			{
				vershifting=vershifting-3.5;
				horshifting=4.1;
			}
	   		
		}
		ijkl=ijkl+1;
	}
	glEndList();
	return loopDL;
}
*/

void renderplayercard2()
{
	int i1=1;
	float horshifting=4.1;
	float vershifting=2.7;
	int noofcards=Monopoly.cities.size()-1;
	while(i1<=noofcards)
	{
		if(p1.citiesowned[whichplayerturn][i1][0]==1)
		{
			glLoadIdentity();
			glTranslatef(-horshifting,vershifting,0);
			glColor3f(1.0,1.0,1.0);
			renderplayercard(i1);
			
				horshifting=horshifting-2.1;
			if(i1%5==0)
			{
				vershifting=vershifting-3.5;
				horshifting=4.1;
			}
	   	
		}
		i1=i1+1;

	}
}



