/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef RENDER_H_
#define RENDER_H_

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>

#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

#define PI 3.141592655
using namespace std;

//extern GLuint textureId[9];


GLMmodel* initObject(const char* filename);
GLuint loadTexture2(Image* image);



#endif
