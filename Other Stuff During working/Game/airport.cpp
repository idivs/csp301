 /*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

#include "render.h"
#include "airport.h"

#define PI 3.141592655
using namespace std;


GLuint textureId[3];

void gametexture(void)
{

	Image* image7 = loadBMP("./images/grass.bmp");
	textureId[0] = loadTexture2(image7);
	delete image7;

	Image* image8 = loadBMP("./images/road.bmp");
	textureId[1] = loadTexture2(image8);
	delete image8;

	Image* image9 = loadBMP("./objects/plane/Texture_0.bmp");
	textureId[2] = loadTexture2(image9);
	delete image9;


}

void rendergrass(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-100.5,0.0,-100.5);
      glTexCoord2f(0.0f, 8.0f);glVertex3f(-100.5,0.0,100.5);
      glTexCoord2f(8.0f, 8.0f);glVertex3f(100.5,0.0,100.5);
      glTexCoord2f(8.0f, 0.0f);glVertex3f(100.5,0.0,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

void renderroad(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[1]);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-2.5,0.05,-100.5);
      glTexCoord2f(0.0f, 1.0f);glVertex3f(-2.5,0.05,2.5);
      glTexCoord2f(1.0f, 1.0f);glVertex3f(2.5,0.05,2.5);
      glTexCoord2f(1.0f, 0.0f);glVertex3f(2.5,0.05,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}


void rendercar(GLMmodel *myModel) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glmFacetNormals(myModel);
	glmVertexNormals(myModel, 1000);
	glmDraw(myModel,GLM_TEXTURE | GLM_MATERIAL |  GLM_SMOOTH);
	glDisable(GL_TEXTURE_2D);
}