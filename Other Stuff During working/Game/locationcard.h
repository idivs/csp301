/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef LOCATIONCARD_H_INCLUDED
#define LOCATIONCARD_H_INCLUDED

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"

extern char dollarsymbol[];
extern GLuint DLcard;

extern char constantrent[];
extern char crent1[];
extern char crent2[];
extern char crent3[];
extern char crent4[];
extern char chotel[];
extern char cmort[];
extern char ccost[];
extern char costwith[];
extern char ch1[];
extern char ch2[];
extern char ch3[];
extern char ch4[];
extern char chostel1[];
extern char dollarsymbol[];

extern GLfloat lengthofcard;
extern GLfloat breadthofcard;	
extern GLfloat cardborder;

void renderlocationcard();
GLuint createcardDL();

#endif