#include "Message.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/asio.hpp>
#include <iostream>

using namespace std;

int main()
{
    bool issend=0;
    Message msg;
    // deserialize

    boost::asio::io_service io_service;
            const uint16_t port = 1234;
            boost::asio::ip::tcp::acceptor acceptor(
                    io_service,
                    boost::asio::ip::tcp::endpoint(
                        boost::asio::ip::address::from_string("127.0.0.1" ),
                        port
                        )
                    );
    	    boost::asio::ip::tcp::socket socket( io_service );
            acceptor.accept( socket );
    for(; ;)
    {
            
            
            std::cout << "connection from " << socket.remote_endpoint() << std::endl;
	    
	    getline(cin,msg._a);
            getline(cin,msg._b);


	    boost::asio::streambuf buf;
            std::ostream os( &buf );
            boost::archive::text_oarchive ar( os );
            ar & msg;

            

            const size_t header = buf.size();
            std::cout << "buffer size " << header << " bytes" << std::endl;
            
            // send header and buffer using scatter
            std::vector<boost::asio::const_buffer> buffers;

            buffers.push_back( boost::asio::buffer(&header, sizeof(header)) );
            buffers.push_back( buf.data() );

            const size_t rc = boost::asio::write(
                    socket,
                    buffers
                    );
            std::cout << "wrote " << rc << " bytes" << std::endl;




            // read header
            size_t header1;
            boost::asio::read(
                    socket,
                    boost::asio::buffer( &header1, sizeof(header1) )
                    );
            std::cout << "body is " << header1 << " bytes" << std::endl;

            // read body
            boost::asio::streambuf buf1;
            const size_t rc1 = boost::asio::read(
                    socket,
                    buf1.prepare( header1 )
                    );


            buf1.commit( header1 );
	    

            std::cout << "read " << rc1 << " bytes" << std::endl;
            std::istream is1( &buf1 );
            boost::archive::text_iarchive ar1( is1 );
            
            
            ar1 & msg;

            std::cout << msg._a << std::endl;
            std::cout << msg._b << std::endl;

            
       
	/*
        else
        {
            msg._a="cool";
            msg._b="divs";


            boost::asio::io_service io_service1;
            boost::asio::ip::tcp::socket socket1( io_service1 );
            const short port1 = 12345;
            socket1.connect(
                boost::asio::ip::tcp::endpoint(
                    boost::asio::ip::address::from_string( "127.0.0.1" ),
                    port1
                    )
                );

            boost::asio::streambuf buf1;
            std::ostream os1( &buf1 );
            boost::archive::text_oarchive ar1( os1);
            ar1 & msg;

            const size_t header1 = buf1.size();
            std::cout << "buffer size " << header1 << " bytes" << std::endl;
            
            // send header and buffer using scatter
            std::vector<boost::asio::const_buffer> buffers1;

            buffers1.push_back( boost::asio::buffer(&header1, sizeof(header1)) );
            buffers1.push_back( buf1.data() );

            const size_t rc1 = boost::asio::write(
                    socket1,
                    buffers1
                    );
            std::cout << "wrote " << rc1 << " bytes" << std::endl;
        
            issend=!issend;
            
        }
  */ 
   }
   

}
    
