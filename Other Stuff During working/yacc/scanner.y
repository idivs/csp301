%{

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "GameInit.h"
extern Game Monopoly;
using namespace std;
extern int yylex();
extern void yyerror(char *s);
void yyerror(const char *str)
{
fprintf(stderr,"error: %s\n",str);
}
%}

%token CURRENCY STARTINGMONEY JAILFINE TAX PERCENT LOCATION LOCATIONTOKEN GROUPTOKEN ROUTE COST RENT

%union{
	std::string *str;
	int num;
}
%token <num> INTEGER
%token <str> WORD

%%
statements:
		| statement statements
		;
statement:
		scurrency
		|
		sstartingmoney
		|
		sjailfine
		|
		stax
		|
		slocation
		|
		sroute
		|
		scost
		|
		srent
		;
scurrency:CURRENCY WORD
			{
				printf("Currency is %s\n",$2->c_str());
				Monopoly.currency=($2);
			}
sstartingmoney:STARTINGMONEY INTEGER
			{
				printf("Each Player starts with %d\n",$2);
				Monopoly.startingmoney=$2;
			}
sjailfine:JAILFINE INTEGER
			{
				printf("Jailfine is %d\n",$2);
				Monopoly.jailfine=$2;
			}
stax:TAX INTEGER PERCENT INTEGER
{
	printf("Tax is set to either %d or is %d percent \n",$2,$4 );
	Monopoly.tax=$2;
}
slocation:LOCATION LOCATIONTOKEN INTEGER WORD GROUPTOKEN INTEGER
{
	City cityobj;
	cityobj.name=($4);
	cityobj.group=$6;
	Monopoly.cities.push_back(cityobj);
	printf("Added city named %s in group %d",cityobj.name->c_str(),cityobj.group);
}
sroute:ROUTE LOCATIONTOKEN INTEGER LOCATIONTOKEN INTEGER
{
	Monopoly.map[$3][$5]=1;
}
scost:COST LOCATIONTOKEN INTEGER INTEGER INTEGER INTEGER INTEGER INTEGER INTEGER INTEGER
{
	Monopoly.cities[$3].cost=$4;
	Monopoly.cities[$3].mortagecost=$5;
	Monopoly.cities[$3].house1cost=$6;
	Monopoly.cities[$3].house2cost=$7;
	Monopoly.cities[$3].house3cost=$8;
	Monopoly.cities[$3].house4cost=$9;
	Monopoly.cities[$3].hotelcost=$10;
}
srent:RENT LOCATIONTOKEN INTEGER INTEGER INTEGER INTEGER INTEGER INTEGER INTEGER
{
	Monopoly.cities[$3].rent0=$4;
	Monopoly.cities[$3].rent1=$5;
	Monopoly.cities[$3].rent2=$6;
	Monopoly.cities[$3].rent3=$7;
	Monopoly.cities[$3].rent4=$8;
	Monopoly.cities[$3].renthotel=$9;	
}
%%

int yywrap()
{
	return 1;
}

extern FILE * yyin;

int finalparser()
{
	yyin=fopen("config.bin","r");
	City nullcity;
	Monopoly.cities.push_back(nullcity);
	yyparse();
	return 1;
}

