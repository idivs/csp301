#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

using namespace std;

GLfloat i=1;

GLuint textureId[6],mainwindow,gamewindow,scorewindow;
GLMmodel *mymodel;
float j=0.0;
int k=1;

#define PI 3.141592655


GLuint loadTexture2(Image* image) {
	
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) ;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0,
			GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return textureId;
}


void dicetexture()
{
	Image* image1 = loadBMP("./objects/hell_pig.bmp");
	textureId[0] = loadTexture2(image1);
	delete image1;

	Image* image2 = loadBMP("./grass.bmp");
	textureId[1] = loadTexture2(image2);
	delete image2;

	Image* image3 = loadBMP("./road.bmp");
	textureId[2] = loadTexture2(image3);
	delete image3;

	Image* image4 = loadBMP("./plane/Texture_0.bmp");
	textureId[3] = loadTexture2(image3);
	delete image4;
}

GLMmodel* initObject(const char* filename) {
	/** initObject: Reads a model description from a Wavefront .OBJ file.
	 * Returns a pointer to the created object which should be free'd with
	 * glmDelete().
	 *
	 * filename - name of the file containing the Wavefront .OBJ format data.
	 *
	 */
	GLMmodel* myModel = glmReadOBJ(filename);
	return myModel;
}


void rendercar(GLMmodel *myModel) {
	/** renderObject: Renders the object file of bike and player with color */
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, textureId[3]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glmFacetNormals(myModel);
	glmVertexNormals(myModel, 1000);
	glmDraw(myModel,GLM_TEXTURE | GLM_MATERIAL |  GLM_SMOOTH);
	glDisable(GL_TEXTURE_2D);
}

void initial()
{
	// glEnable(GL_CULL_FACE);
 //  	glCullFace(GL_BACK);
	glClearColor(0.0,0.0,0.0,0.3);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/** Function used to load the required objects and textures into memory */
	glEnable(GL_COLOR_MATERIAL);
	//glEnable(GL_LIGHTING);
	 //glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	mymodel = initObject("./plane/plane.obj");
	//mymodel = initObject("./objects/hell_pig.obj.txt");
}
void reshape_game (int width , int height)
{

	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-10.0f);				// Move Right 1.5 Units And Into The Screen 6.0
	glRotatef(10,1.0f,0.0f,0.0f);
	dicetexture();
	initial();

}
void displaygame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
	glColor3f(1.0,1.0,1.0);	

	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-100.5,0.0,-100.5);
      glTexCoord2f(0.0f, 8.0f);glVertex3f(-100.5,0.0,100.5);
      glTexCoord2f(8.0f, 8.0f);glVertex3f(100.5,0.0,100.5);
      glTexCoord2f(8.0f, 0.0f);glVertex3f(100.5,0.0,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);

    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[2]);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-2.5,0.05,-100.5);
      glTexCoord2f(0.0f, 1.0f);glVertex3f(-2.5,0.05,2.5);
      glTexCoord2f(1.0f, 1.0f);glVertex3f(2.5,0.05,2.5);
      glTexCoord2f(1.0f, 0.0f);glVertex3f(2.5,0.05,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
   
   glPushMatrix();
   if(j<100)
   {
   		k=k+0.9;
   		j=(j+0.07)*k;
   		glTranslatef(-0.5, 0.30+j/5, -1.5-j);
   		glScalef(0.004,0.004,0.004);
    	rendercar(mymodel);
    	glutPostRedisplay();
    	glutSwapBuffers();
    	
    }
    glPopMatrix();
    
    

	
	
}

void reshape_score (int width, int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	//luPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void displayscore()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glutSwapBuffers();
}





void reshape_main(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	//gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	//glTranslatef(0.0f,0.0f,-10.0f);				// Move Right 1.5 Units And Into The Screen 6.0
	//glRotatef(10,1.0f,0.0f,0.0f);
}

void displaymain()
{
	glClearColor(0.8, 0.8, 0.8, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glutSwapBuffers();
}

/*
void redisplay_all(void)
{
    glutSetWindow(gamewindow);
    glutPostRedisplay();
    glutSetWindow(scorewindow);
    glutPostRedisplay();

}
*/
int main(int argc,char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_DEPTH | GLUT_RGBA | GLUT_ALPHA);
	glutInitWindowSize(1200,700);
	glutInitWindowPosition(10,10);

	mainwindow = glutCreateWindow("Monopoly");
	glutDisplayFunc(displaymain);
	glutReshapeFunc(reshape_main);

	gamewindow = glutCreateSubWindow(mainwindow,0,0,900,900);
	glutReshapeFunc(reshape_game);
	glutDisplayFunc(displaygame);
	

	scorewindow = glutCreateSubWindow(mainwindow,900,900,300,900);
	glutReshapeFunc(reshape_score);
	glutDisplayFunc(displayscore);
	
	//dicetexture();
	//initial();
	
	//glutIdleFunc(renderScene);
	//redisplay_all();
	glutMainLoop();
	return 0;
}