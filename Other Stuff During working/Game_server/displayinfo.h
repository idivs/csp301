/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef DISPLAYINFO_H_INCLUDED
#define DISPLAYINFO_H_INCLUDED

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>

void displaymouseclicklocation();

#endif