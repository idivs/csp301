
/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef AIRPORT_H_
#define AIRPORT_H_

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>

#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>
#include "render.h"

void gametexture(void);
void rendergrass(void);
void renderroad(void);
void rendercar(GLMmodel *myModel);

#endif