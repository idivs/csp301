#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files




#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/vector.hpp>

#include <boost/serialization/vector.hpp>

#include <boost/serialization/list.hpp>
#include <boost/serialization/string.hpp>
#include <fstream>
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"
#include "Player.h"
#include "Gameplay.h"
#include "bot.h"

using namespace std;
#define PI 3.141592655

extern int finalparser();
GLMmodel *mymodel; //to load Model





float playerj=0.0;

int w;
int h;
float cardy=0;
float playery=0;
int carddisplayflag =0;
int playerdisplayflag=0;
int lastdisplayflag=0;

bool filtercard=0;
bool filterdice=0;
bool filterplayer=0;
bool filtergame=0;
bool filterglobe=1;


GLuint DLid1;
Game Monopoly;

void generatediceface()
{
	srand(time(NULL));
	facevalue = (rand() % 6) + 1;
	remainingvalueofdice=facevalue;
}

GLuint creategameDL() 
{
	GLuint planeDL;
	planeDL = glGenLists(1);
	glNewList(planeDL,GL_COMPILE);
	rendercar(mymodel);
	glEndList();
	return(planeDL);
}

void initial_game()
{
	glClearColor(0.0,0.0,0.0,0.3);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	mymodel = initObject("./objects/plane/plane.obj");
	gametexture();

	citymodel = initObject("./objects/City4/t.obj");
	DLid1 = creategameDL();
}

void reshape_game (int width , int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,1000.0f);   	// Calculate The Aspect Ratio Of The Window
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-20.0f);				// Move Right 1.5 Units And Into The Screen 6.0
	glRotatef(10,1.0f,0.0f,0.0f);
}
void displaygame()
{
	if(showplanetransition==1)
	{

		if(currentdisplayflag==0)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==1)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==2)
		{

			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==3)
		{
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutShowWindow();
		}	
		glutSetWindow(gamewindow);
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
		glColor3f(1.0,1.0,1.0);
		glColor4f(1,1,1,0);
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-20.0f);				// Move Right 1.5 Units And Into The Screen 6.0
		glRotatef(10,1.0f,0.0f,0.0f);
		GLfloat sizeofspace=50;
		renderroad();
		rendergrass();
		//glLoadIdentity();
	    glPushMatrix();
	   	if(jplane<10)
	   	{
	   		kplane=kplane+1;
	   		jplane=(jplane+0.02);
	   		glTranslatef(-0.5, 0.30+jplane/5, -1.5-kplane*1.0f/8);
	   		glScalef(0.004,0.004,0.004);
	    	glCallList(DLid1);
	    	glutPostRedisplay();
	    	
	   	}
	   	else
	   	{

	   		Monopoly.currentlocation[Monopoly.whichplayerturn]=Monopoly.temporarylocation[Monopoly.whichplayerturn];

			playerwhocanbuy=Monopoly.whichplayerturn;
	   		showplanetransition=0;
	   		currentdisplayflag=0;
	   		if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==0)
	   		{
	   			locationbuytag=1;
	   		}
	   		else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel==0 && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby==playerwhocanbuy)
	   		{
	   			locationbuytag=1;
	   		}
	   		else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel==1 && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby==playerwhocanbuy)
	   		{
				isdicerolled=0;
				currentdisplayflag=3;
				locationbuytag=0;
				userwantstobuy=0;
				Monopoly.whichplayerturn=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
	   		}
	   		else
	   		{
	   			/*Rent Code will come here */
	   		}

	   	}
	   	glPopMatrix();
	   	glutSwapBuffers();
	}
	else
	{
		if(currentdisplayflag==0)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==1)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==2)
		{

			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==3)
		{
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutShowWindow();
		}
		//cout << "Player has been transported to : " << Monopoly.currentlocation << endl;/
		//cout << locationbuytag << endl;
		
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glutSetWindow(gamewindow);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer

		glLoadIdentity();
		//glScalef(0.01,0.01,0.01);
		//glTranslatef(0,-10,-100);
		glTranslatef(0.0f,0.0f,10.0f);				// Move Right 1.5 Units And Into The Screen 6.0
		glRotatef(-10,1.0f,0.0f,0.0f);
		// /glScalef(0.01,0.01,0.01);
		glTranslatef(100.0f,0.0f,-599.0f);
		glColor3f(1.0,1.0,1.0);
		loadcityingame();
		if(locationbuytag==1)
		{	
			//cout << locationbuytag << endl;
			if(userwantstobuy==1)
			{
				//cout <<"bye bye bye " << endl;
				buylocation();
				//DLplayer = createplayerDL();
				locationbuytag=0;
				userwantstobuy=0;
				isdicerolled=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
				currentdisplayflag=3;
				if(Monopoly.whichplayerturn==0)
				{
					Monopoly.whichplayerturn=0;
				}
				else
				{
					Monopoly.whichplayerturn++;
				}
				
			}

			if(userwantstobuy==-1)
			{
				isdicerolled=0;
				currentdisplayflag=3;
				locationbuytag=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
				userwantstobuy=0;
				if(Monopoly.whichplayerturn==2)
				{
					Monopoly.whichplayerturn=0;
					
				}
				else
				{
					Monopoly.whichplayerturn++;
				}
				
			}
		}
		//cout << "hi1235555" << endl;
		glutPostRedisplay();	
		glutSwapBuffers();
	}

    
}

void initial_dice()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	// glClearColor(0.5, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);

}
void reshape_dice (int width, int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
}

void displaysdice()
{

	if(Monopoly.isturncompleted[Monopoly.whichplayerturn]==0 && diceflag==0 && isdicerolled==0)
	{
		generatediceface();
		isdicerolled=1;
	    cout << facevalue << endl;
	}
	
	else if(Monopoly.whichplayerturn==0 && isdicerolled==0)
	{
		diceflag=0;
		Monopoly.isturncompleted[Monopoly.whichplayerturn]=0;
		xrotation=0;
		yrotation=0;
		zrotation=0;
		generatediceface();
		isdicerolled=1;
	    cout << facevalue << endl;

	    findnodesatdistancek(Monopoly.currentlocation[0],facevalue);
	    int whichtobuy=mainaiforbot();

	    Monopoly.temporarylocation[0]=whichtobuy;
	    int endlocationofplayer=Monopoly.temporarylocation[Monopoly.whichplayerturn];
        currentdisplayflag=0;
        showplanetransition=1;
        kplane=0;
        jplane=0;
	}
	//else if()
	// if(diceflag==0)
	// {
	// 	if(isdicerolled==0)
	// 	{
	// 		generatediceface();
	// 		cout << facevalue << endl;
	// 		isdicerolled=1;
	// 	}
	// }
		glutSetWindow(dicewindow);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices									// Select The Modelview Matrix
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glRotatef(xrotation,0.5,0.0,0.0);		// Rotation about x-axis
		glRotatef(yrotation,0.0,0.5,0.0);       // Rotation about y-axis
		glRotatef(zrotation,0.0,0.0,0.5);		// Rotation about z-axis
		// if(diceflag==0)
		// {
		// 	if(isdicerolled==0)
		// 	{
		// 		generatediceface();
		// 		cout << facevalue << endl;
		// 		isdicerolled=1;
		// 	}
		// }
		// if(hasgamestarted==0 && diceflag==0)
		// {
		// 	generatediceface();
		// 	cout << facevalue << endl;
		// 	hasgamestarted=1;
		// 	isdicerolled=1;	
		// }
		//glRotatef(90,0.5,0.0,0.0);	
		//glEnable( GL_CULL_FACE );
	    //glCullFace ( GL_BACK );
	    setdicevalue();
		renderdice();
		glutPostRedisplay();
		glutSwapBuffers();
}


void reshape_card(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}
void initial_card()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	DLcard = createcardDL();
	
}

void display_card()
{

	if(currentdisplayflag==0)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==1)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==2)
	{

		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==3)
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutShowWindow();
	}
	// if(carddisplayflag==1)
	// {
	// 	glutSetWindow(gamewindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutplayerWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutShowWindow();
	// }
	/*
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	*/
	// if(playerdisplayflag==1)
	// {
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutShowWindow();
	// }
	// else
	// {
	// 	glutSetWindow(playerwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(gamewindow);
	// 	glutShowWindow();
	// }
	
	glutSetWindow(cardwindow);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glMatrixMode(GL_PROJECTION);
	gluLookAt( 0,cardy,0,
	0, cardy,-1 ,
	0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glCallList(DLcard);
	glutPostRedisplay();
   	glutSwapBuffers();
}

void reshape_player(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}
void initial_player()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	//DLplayer = createplayerDL();
	
}

void display_player()
{
	if(currentdisplayflag==0)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==1)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==2)
	{

		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==3)
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutShowWindow();
	}
	// if(carddisplayflag==1)
	// {
	// 	glutSetWindow(gamewindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutplayerWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutShowWindow();
	// }
	/*
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	*/
	// if(playerdisplayflag==1)
	// {
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutShowWindow();
	// }
	// else
	// {
	// 	glutSetWindow(playerwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(gamewindow);
	// 	glutShowWindow();
	// }
	
	glutSetWindow(playerwindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glMatrixMode(GL_PROJECTION);
	gluLookAt( 0,cardy,0,
	0, cardy,-1 ,
	0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	
	//DLplayer = createplayerDL();
	//glCallList(DLplayer);
	renderplayercard2();
	glutPostRedisplay();
   	glutSwapBuffers();
}

void reshape_money(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	// Move Right 1.5 Units And Into The Screen 6.0
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
}

void initial_money()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	// glClearColor(0.5, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);

}

void display_money()
{
	glutSetWindow(moneywindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	//glPushMatrix();										// Select The Modelview Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	displaycurrentamount();
	glutPostRedisplay();
	glutSwapBuffers();
}


void resize_info(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height*0.8,1.0f,100.0f); 

	gluLookAt( 0,0,0.0,
	0, 0,-1 ,
	0.0f, 1.0f, 0.0f);  	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	glTranslatef(0,2,0);
	glScalef(1.5,1.5,1);
}

void init_info()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//glClearColor(0.2, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void render_info()
{
	glutSetWindow(infowindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	

	glMatrixMode(GL_MODELVIEW);

	if(selectedsnowman!=0)
	{
	  //cout << "hi";
	  displaymouseclicklocation();
	}
	if(selectedsnowman==0)
	{ 
		glColor3f(1,1,1);
		char startgamedisplay[]="STARTPOINT --> You get Fixed $$ everytime you arrive here";
		renderBitmapString(-1.6f, 0.55f, -9.989f, (void *)font ,startgamedisplay);
	}
	glutPostRedisplay();
	glutSwapBuffers();
}


void processSpecialKeys(int key, int xx, int yy) 
{

	float fraction = 0.4f;
	float fraction1 = 0.05f;
	float fraction2=1.0f;
	if(currentdisplayflag==1)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
				cardy=cardy+fraction;
				break;
			case GLUT_KEY_DOWN :
				cardy=cardy-fraction;
				break;
		}
	}
	else if(currentdisplayflag==2)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
				playery=playery+fraction1;
				break;
			case GLUT_KEY_DOWN :
				playery=playery-fraction1;
				break;
		}
	}
	else
	{
		switch(key)
		{
			case GLUT_KEY_LEFT :
				startleftrotation=1;
				break;
			case GLUT_KEY_RIGHT :
				startrightrotation=1;
				break;
			case GLUT_KEY_UP :
				startuprotation=1;
				break;
			case GLUT_KEY_DOWN :
				startdownrotation=1;
				break;
		}
	}
}
	

void releaseKey1(int key, int x, int y){
	if(currentdisplayflag==1)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
			case GLUT_KEY_DOWN : cardy = 0;break;
		}
	}
	else if(currentdisplayflag==2)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
			case GLUT_KEY_DOWN : playery = 0;break;
		}
	}
	else
	{	
		switch (key) 
		{
			case GLUT_KEY_LEFT :
				startleftrotation=0;
				break;
			case GLUT_KEY_RIGHT :
				startrightrotation=0;
				break;
			case GLUT_KEY_UP :
				startuprotation=0;
				break;
			case GLUT_KEY_DOWN :
				startdownrotation=0;
				break;
		}
	}
}

void processNormalKeys(unsigned char key, int x, int y) {
	
	if(key == 'r')
	{

	}
	else if(key=='b')
	{

	}

	else if (key == 'g')
	{
		if(filtergame==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=0;
			//cout << carddisplayflag << endl;
		}
		if(filtergame==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}

	else if (key == 's')
	{
		if(filtercard==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=1;
			//cout << carddisplayflag << endl;
		}
		if(filtercard==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}
	else if (key == 'p')
	{

		if(filterplayer==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=2;

			//cout << carddisplayflag << endl;
		}
		if(filterplayer==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}
	else if (key == 'l')
	{
		if(filterglobe==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=3;
			//cout << carddisplayflag << endl;
		}
		if(filterglobe==1)
		{
			//currentdisplayflag=lastdisplayflag;
			currentdisplayflag=0;
		}
	}
}


void releaseKey(unsigned char key, int x, int y) 
{

	if (key == 'g')
	{
		filtergame=!filtergame;
	}
	if (key == 's')
	{
		filtercard=!filtercard;
	}
	if (key == 'p')
	{
		filterplayer=!filterplayer;
	}
	if (key == 'l')
	{
		filterglobe=!filterglobe;
	}
	if(key == 'r')
	{
		if(Monopoly.whichplayerturn==whichplayeristhis)
		{
			hasgamestarted=1;
			diceflag=0;
			Monopoly.isturncompleted[Monopoly.whichplayerturn]=0;
			xrotation=0;
			yrotation=0;
			zrotation=0;
		}
	}
	if(key=='y')
	{
		if(playerwhocanbuy==Monopoly.whichplayerturn)
		{
			userwantstobuy=1;
		}
	}
	if(key=='n')
	{
		if(playerwhocanbuy==Monopoly.whichplayerturn)
		{
			userwantstobuy=-1;
		}
	}
}


void reshape_main(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}

void displaymain()
{	
	
	glutSetWindow(mainwindow);
	// glClearColor(0.9, 0.8, 0.8, 0.0);
	glClearColor(0, 0, 0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glutPostRedisplay();
   	glutSwapBuffers();
}

int main(int argc,char** argv)
{
	

	cout <<	finalparser() << endl;
	Monopoly.Generatecolorgroups();
	srand (time(NULL));
	generaterandompoints();
	initialiseplayer();
	findnodesatdistancek(0,2);

	{
		// create and open a character archive for output
	    std::ofstream ofs("filename");

	     boost::archive::text_oarchive oa(ofs);
	        // write class instance to archive
	        oa << Monopoly;
	    	// archive and stream closed when destructors are called
	}

        Game newg;
    	 // create and open an archive for input
        std::ifstream ifs("filename");
        boost::archive::text_iarchive ia(ifs);
        // read class state from archive
        ia >> newg;
        // archive and stream closed when destructors are called

	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowPosition(10,10);
	glutInitWindowSize(1200,700);
	
	mainwindow = glutCreateWindow("Monopoly");
	glutDisplayFunc(displaymain);
	glutMouseFunc(mouseStuff);
		//glutReshapeFunc(reshape_main);

	globewindow = glutCreateSubWindow(mainwindow,900,120,300,250);
	glutReshapeFunc(resize_globe);
	glutDisplayFunc(render_globe);
	glutMouseFunc(mouseStuff);
	//glutTimerFunc(25,update,0);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_globe();

	globemainwindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(resize_globe);
	glutDisplayFunc(render_globe);
	glutMouseFunc(mouseStuff);
	//glutTimerFunc(25,update,0);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_globe();


	infowindow = glutCreateSubWindow(mainwindow,900,370,300,400);
	glutReshapeFunc(resize_info);
	glutDisplayFunc(render_info);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_info();

	gamewindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(reshape_game);
	glutDisplayFunc(displaygame);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	
	initial_game();
	
	dicewindow = glutCreateSubWindow(mainwindow,900,0,150,120);
	glutReshapeFunc(reshape_dice);
	glutDisplayFunc(displaysdice);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	
	
	initial_dice();

	moneywindow = glutCreateSubWindow(mainwindow,1050,0,150,120);
	glutReshapeFunc(reshape_money);
	glutDisplayFunc(display_money);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(releaseKey);
	
	initial_money();
	
	cardwindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(reshape_card);
	glutDisplayFunc(display_card);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);
	initial_card();

	playerwindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(reshape_player);
	glutDisplayFunc(display_player);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);
	initial_player();


	glutMainLoop();
	return 0;
}
