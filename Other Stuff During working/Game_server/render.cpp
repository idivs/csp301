/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>
#include "render.h"

#define PI 3.141592655
using namespace std;


GLMmodel* initObject(const char* filename) {
	/** initObject: Reads a model description from a Wavefront .OBJ file.
	 * Returns a pointer to the created object which should be free'd with
	 * glmDelete().
	 *
	 * filename - name of the file containing the Wavefront .OBJ format data.
	 *
	 */
	GLMmodel* myModel = glmReadOBJ(filename);
	return myModel;
}

GLuint loadTexture2(Image* image) {
	
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) ;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0,
			GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return textureId;
}





