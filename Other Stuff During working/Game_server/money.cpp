#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"
#include "money.h"
#include "Player.h"

extern Game Monopoly;
float usermoney=32.2;
char displaymoney[30];
char hitext[]="You Have : ";

void displaycurrentamount()
{
	snprintf(displaymoney, sizeof(displaymoney), "%f", Monopoly.hismoney[whichplayeristhis]);
	renderBitmapString(-0.8f, 1.2f, 0.0f, (void *)font ,hitext);
	renderBitmapString(-1.0f, 0.5f, 0.0f, (void *)font ,dollarsymbol);
	renderBitmapString(-0.8f, 0.5f, 0.0f, (void *)font ,displaymoney);
}