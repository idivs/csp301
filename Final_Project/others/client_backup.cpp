#include "Message.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/asio.hpp>

#include <iostream>

using namespace std;

int histurn=1;
bool issend=1;
int main()
{

    Message msg;
    //msg._a = "hello";
    //msg._b = "world";
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket( io_service );
    const short port = 1234;
    for(; ;)
    {
                
                socket.connect(
                        boost::asio::ip::tcp::endpoint(
                            boost::asio::ip::address::from_string( "10.0.0.4" ),
                            port
                            )
                        );

            if(histurn==1)
            {
                getline(cin,msg._a);
                getline(cin,msg._b);

                boost::asio::streambuf buf;
                std::ostream os( &buf );
                boost::archive::text_oarchive ar( os );
                ar & msg;

                

                const size_t header = buf.size();
                std::cout << "buffer size " << header << " bytes" << std::endl;
                
                // send header and buffer using scatter
                std::vector<boost::asio::const_buffer> buffers;

                buffers.push_back( boost::asio::buffer(&header, sizeof(header)) );
                buffers.push_back( buf.data() );

                const size_t rc = boost::asio::write(
                        socket,
                        buffers
                        );
                std::cout << "wrote " << rc << " bytes" << std::endl;
                histurn=0;
            }

                
                while(histurn!=1)
                {
                    
                        size_t header1;
                        boost::asio::read(
                        socket,
                        boost::asio::buffer( &header1, sizeof(header1) )
                         );
                        std::cout << "body is " << header1 << " bytes" << std::endl;


                        // read body
                        boost::asio::streambuf buf1;
                        const size_t rc1 = boost::asio::read(
                                socket,
                                buf1.prepare( header1 )
                                );


                        buf1.commit( header1);

                        std::cout << "read " << rc1 << " bytes" << std::endl;
                        std::istream is1( &buf1 );
                        boost::archive::text_iarchive ar1( is1);
                        
                        
                        ar1 & msg;

                        std::cout << msg._a << std::endl;
                        std::cout << msg._b << std::endl;
                }

                //socket.shutdown(boost::asio::ip::tcp::socket::shutdown_send);
            
    }

       

        
  
}