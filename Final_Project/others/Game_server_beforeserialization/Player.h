
/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef PLAYER_H_
#define PLAYER_H_


#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"

extern GLuint DLplayer;
extern GLuint createplayerDL();
void renderplayercard2();
void initialiseplayer();


class Player
{
public:
	string *name[5];
	float hismoney[5];
	int citiesowned[5][100][5];
	int noofcitiesowned[5];
	int citiesofeachgroup[5][50];
	int currentlocation[5];
	int temporarylocation[5];
};

#endif