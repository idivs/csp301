/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef TEXTDISPLAY_H_INCLUDED
#define TEXTDISPLAY_H_INCLUDED

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
extern void *font;
extern void *font1;
extern void *font2;
	void renderBitmapString(
		float x,
		float y,
		float z,
		void *font,
		char *string);
#endif