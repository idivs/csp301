#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"
#include "Player.h"
#include "Gameplay.h"
GLMmodel *citymodel;

extern Game Monopoly;
int locationbuytag=0;
int userwantstobuy=0;
void loadcityingame()
{
	glmFacetNormals(citymodel);
	glmVertexNormals(citymodel, 1000);
	glmDraw(citymodel,GLM_MATERIAL |  GLM_SMOOTH);
}

void buylocation()
{
	
	if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought!=1)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].cost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].cost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby=Monopoly.whichplayerturn;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][0]=1;
			//cout << "HI there : " << Monopoly.citiesowned[Monopoly.currentlocation[Monopoly.whichplayerturn]][0] << endl;
			Monopoly.noofcitiesowned[Monopoly.whichplayerturn]=Monopoly.noofcitiesowned[Monopoly.whichplayerturn]+1;
			Monopoly.citiesofeachgroup[Monopoly.whichplayerturn][Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].group]=Monopoly.citiesofeachgroup[Monopoly.whichplayerturn][Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].group]+1;
		}
	}
	else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==1 && Monopoly.whichplayerturn==Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses==0)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house1cost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house1cost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][1]=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==1 && Monopoly.whichplayerturn==Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses==1)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house2cost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house2cost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][2]=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==1 && Monopoly.whichplayerturn==Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses==2)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house3cost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house3cost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][3]=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==1 && Monopoly.whichplayerturn==Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses==3)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house4cost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].house4cost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][4]=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].totalhouses+1;
		}
	}
	else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==1 && Monopoly.whichplayerturn==Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel==0)
	{
		if(Monopoly.hismoney[Monopoly.whichplayerturn]>=Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].hotelcost)
		{
			Monopoly.hismoney[Monopoly.whichplayerturn]=Monopoly.hismoney[Monopoly.whichplayerturn]-Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].hotelcost;
			cout << "His money is : " << Monopoly.hismoney[Monopoly.whichplayerturn] << endl;
			Monopoly.citiesowned[Monopoly.whichplayerturn][Monopoly.currentlocation[Monopoly.whichplayerturn]][5]=1;
			Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel=1;
		}
	}


}