#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>

using namespace std;

GLuint mainwindow,gamewindow,dicewindow,moneywindow,cardwindow;
GLMmodel *mymodel;
float j=0.0;
int k=1;
int facevalue=5;
int w;
int h;
int diceflag=0;

float cardy=0;
// width and height of the window
GLfloat xrotation=1;

GLfloat yrotation=1;
GLfloat zrotation=1;

#define PI 3.141592655

float usermoney=32.2;
int carddisplayflag =0;
bool filtercard=0;
bool filterdice=0;
bool isdicecomplete=0;
void *font = GLUT_BITMAP_HELVETICA_10;
void *font1 = GLUT_BITMAP_HELVETICA_12;


void renderBitmapString(
		float x,
		float y,
		float z,
		void *font,
		char *string) {
  char *c;
  glRasterPos3f(x, y,z);
  for (c=string; *c != '\0'; c++) {
    glutBitmapCharacter(font, *c);
  }
}

void generatediceface()
{
	srand(time(NULL));
	facevalue = (rand() % 6) + 1;
}


void initial_game()
{
	// glEnable(GL_CULL_FACE);
 //  	glCullFace(GL_BACK);
	glClearColor(0.0,0.0,0.0,0.3);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/** Function used to load the required objects and textures into memory */
	glEnable(GL_COLOR_MATERIAL);
	//glEnable(GL_LIGHTING);
	 //glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	mymodel = initObject("./objects/plane/plane.obj");
	gametexture();
	//mymodel = initObject("./objects/hell_pig.obj.txt");
}

void reshape_game (int width , int height)
{

	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-10.0f);				// Move Right 1.5 Units And Into The Screen 6.0
	glRotatef(10,1.0f,0.0f,0.0f);
}
void displaygame()
{
	if(carddisplayflag==1)
	{
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}

	glutSetWindow(gamewindow);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
	glColor3f(1.0,1.0,1.0);	

	rendergrass();
	renderroad();
   
   glPushMatrix();
   if(j<100)
   {
   		k=k+0.9;
   		j=(j+0.07)*k;
   		glTranslatef(-0.5, 0.30+j/5, -1.5-j);
   		glScalef(0.004,0.004,0.004);
    	rendercar(mymodel);
    	glutPostRedisplay();
    	glutSwapBuffers();
    	
    }
    glPopMatrix();
}
void initial_dice()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
}
void reshape_dice (int width, int height)
{

	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	// Move Right 1.5 Units And Into The Screen 6.0
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();

}


void displaysdice()
{
	//cout << "hi1234";
	if(diceflag==0)
	{
		generatediceface();
		cout << facevalue << endl;
	}


	glutSetWindow(dicewindow);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices									// Select The Modelview Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(xrotation,0.5,0.0,0.0);		// Rotation about x-axis
	glRotatef(yrotation,0.0,0.5,0.0);       // Rotation about y-axis
	glRotatef(zrotation,0.0,0.0,0.5);		// Rotation about z-axis

	//glRotatef(90,0.5,0.0,0.0);	
	//glEnable( GL_CULL_FACE );
    //glCullFace ( GL_BACK );

	if(facevalue==1)
	{
		diceflag=1;
		if(yrotation<360.00)
		{
			xrotation=xrotation+6;
			yrotation=yrotation+6;
			zrotation=zrotation+6;
			//cout << i << endl;
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}
	}


	if(facevalue==2)
	{
		diceflag=1;
		if(xrotation<90.00)
		{
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;
			//cout << xrotation << endl;
			//cout << i << endl;

			
			
			
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}
	}

	if(facevalue==3)
	{
		diceflag=1;
		if(yrotation<270)
		{
			xrotation=xrotation+8;
			yrotation=yrotation+2;
			zrotation=zrotation+8;
			
			
		}
		else
		{
			isdicecomplete=1;
		}
	}
	if(facevalue==4)
	{
		diceflag=1;
		if(yrotation<90)
		{
			xrotation=xrotation+8;
			yrotation=yrotation+2;
			zrotation=zrotation+8;
			
		}
		else
		{
			isdicecomplete=1;
		}
	}
	if(facevalue==5)
	{
		diceflag=1;
		if(xrotation<270)
		{
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;

			
			
		}
		else
		{
			isdicecomplete=1;
		}
	}
	
	if(facevalue==6)
	{
		diceflag=1;
		if(xrotation<180.00)
		{	
			xrotation=xrotation+2;
			yrotation=yrotation+8;
			zrotation=zrotation+8;
			
			
			//cout << xrotation << " " << yrotation << endl;
			//generatediceface();
		}
		else
		{
			isdicecomplete=1;
		}
		
		
	}
	if(isdicecomplete==1)
	{
		/*Font with the text will be displayed here */
			//cout << "The value is : " << facevalue;
	}
	renderdice();
	glutPostRedisplay();
	glutSwapBuffers();
}


void reshape_card(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}

void initial_card()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void display_card()
{

	if(carddisplayflag==1)
	{
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}

	GLfloat lengthofcard=1.3;
	GLfloat breadthofcard=1.0;	
	GLfloat cardborder=0.2;
	glutSetWindow(cardwindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glMatrixMode(GL_PROJECTION);
	gluLookAt( 0,cardy,0,
	0, cardy,-1 ,
	0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	int i=1;
	float horshifting=4.1;
	float vershifting=2.7;
	int noofcards=28;
	
	while(i<noofcards)
	{
		char constantrent[]="Rent With";
		char crent1[]="1 House : ";
		char crent2[]="2 Houses : ";
		char crent3[]="3 Houses : ";
		char crent4[]="4 Houses : ";
		char chotel[]="Hotel : ";

		char cmort[]="Mortage Value :";
		char ccost[]="Buying Cost : ";
		char costwith[]="Cost to Buy : ";
		char ch1[]="1st House : ";
		char ch2[]="2nd House : ";
		char ch3[]="3rd House : ";
		char ch4[]="4th House : ";
		char chostel1[]="Hotel : ";



		char dollarsymbol[]="$";
		char propertyname[50]="Meditarrian Avenue";
		char properycost[30] = "300";
		char costwithh1[]="100";
		char costwithh2[]="100";
		char costwithh3[]="100";
		char costwithh4[]="100";
		char costwithhotel[]="200";
		char rent1[30]="50";
		char rent2[30]="100";
		char rent3[30]="200";
		char rent4[30]="200";
		char renthotel[30]="250";
		char mortage[30]="75";

		glLoadIdentity();
		glTranslatef(-horshifting,vershifting,0);
		glColor3f(1.0,1.0,1.0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,textureId[0]);
		glBegin(GL_QUADS);
	    glTexCoord2f(0.0f, 0.0f); glVertex3f(-breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 0.0f); glVertex3f( breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 1.0f); glVertex3f( breadthofcard,  lengthofcard,  -10);  
	    glTexCoord2f(0.0f, 1.0f); glVertex3f(-breadthofcard,  lengthofcard,  -10); 
	   	glEnd();

	   	glDisable(GL_TEXTURE_2D);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.999);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.999); 
	    
	   	glEnd();
	   	glColor3f(0.984,0.8352,0.611);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.990);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.990); 
	   	glEnd();

	   	glColor3f(0.0,0.0,0.0);
	   	renderBitmapString(-0.7f, 0.8f, -9.989f, (void *)font1 ,propertyname);

	   	renderBitmapString(-0.7f, 0.55f, -9.989f, (void *)font ,ccost);
	   	renderBitmapString(0.5f, 0.55f, -9.989f, (void *)font ,properycost);

	   	renderBitmapString(-0.7f, 0.35f, -9.989f, (void *)font ,ch1);
	   	renderBitmapString(0.5f, 0.35f, -9.989f, (void *)font ,costwithh1);

	   	renderBitmapString(-0.7f, 0.15f, -9.989f, (void *)font ,ch2);
	   	renderBitmapString(0.5f, 0.15f, -9.989f, (void *)font ,costwithh2);

	   	renderBitmapString(-0.7f, -0.05f, -9.989f, (void *)font ,ch3);
	   	renderBitmapString(0.5f, -0.05f, -9.989f, (void *)font ,costwithh3);

	   	renderBitmapString(-0.7f, -0.25f, -9.989f, (void *)font ,ch4);
	   	renderBitmapString(0.5f, -0.25f, -9.989f, (void *)font ,costwithh4);

	   	renderBitmapString(-0.7f, -0.45f, -9.989f, (void *)font ,chotel);
	   	renderBitmapString(0.5f, -0.45f, -9.989f, (void *)font ,costwithhotel);

	   	renderBitmapString(-0.3f, -0.65f, -9.989f, (void *)font1 ,constantrent);

	   	renderBitmapString(-0.7f, -0.85f, -9.989f, (void *)font ,crent1);
	   	renderBitmapString(0.5f, -0.85f, -9.989f, (void *)font ,rent1);

	   	renderBitmapString(-0.7f, -1.05f, -9.989f, (void *)font ,crent2);
	   	renderBitmapString(0.5f, -1.05f, -9.989f, (void *)font ,rent2);

	   	renderBitmapString(-0.7f, -1.25f, -9.989f, (void *)font ,crent3);
	   	renderBitmapString(0.5f, -1.25f, -9.989f, (void *)font ,rent3);

	   	renderBitmapString(-0.7f, -1.45f, -9.989f, (void *)font ,crent4);
	   	renderBitmapString(0.5f, -1.45f, -9.989f, (void *)font ,rent4);

	   	renderBitmapString(-0.7f, -1.65f, -9.989f, (void *)font ,chostel1);
	   	renderBitmapString(0.5f, -1.65f, -9.989f, (void *)font ,renthotel);

	   	renderBitmapString(-0.7f, -1.85f, -9.989f, (void *)font ,cmort);
	   	renderBitmapString(0.5f, -1.85f, -9.989f, (void *)font ,mortage);



	   	

	   	//renderBitmapString(-0.9f, 0.5f, -9.989f, (void *)font1 ,rent1);
	   	//renderBitmapString(-0.7f, 0.8f, -9.989f, (void *)font1 ,propertyname);
	   	horshifting=horshifting-2;

	   	if(i%5==0)
		{
			vershifting=vershifting-3.5;
			horshifting=4.1;
		}
	   	i=i+1;
	}

   	glutSwapBuffers();
}




void reshape_money(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	// Move Right 1.5 Units And Into The Screen 6.0
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
}

void initial_money()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	glClearColor(0.3, 1.0, 1.0, 0.0);
}

void display_money()
{
	char hitext[]="You Have : ";
	char dollarsymbol[]="$";
	char displaymoney[30];

	snprintf(displaymoney, sizeof(displaymoney), "%f", usermoney);
	GLfloat sizeofdice=0.3;	
	glutSetWindow(moneywindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	//glPushMatrix();										// Select The Modelview Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);

	// All the Quadrilaterals are made in counter-clockwise fashion.

	// Front Face with "1"

	renderBitmapString(-0.8f, 1.2f, 0.0f, (void *)font ,hitext);
	renderBitmapString(-1.0f, 0.5f, 0.0f, (void *)font ,dollarsymbol);
	renderBitmapString(-0.8f, 0.5f, 0.0f, (void *)font ,displaymoney);
	glBindTexture(GL_TEXTURE_2D,textureId[0]);
	glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofdice, -sizeofdice,  sizeofdice);  
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofdice,  sizeofdice,  sizeofdice);  
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofdice,  sizeofdice,  sizeofdice); 
   	glEnd();
	glDisable(GL_TEXTURE_2D);
	glutSwapBuffers();


}

void processSpecialKeys(int key, int xx, int yy) {

	float fraction = 0.05f;

	switch (key) {
		case GLUT_KEY_UP :
			cardy=cardy+fraction;
			break;
		case GLUT_KEY_DOWN :
			cardy=cardy-fraction;
			break;
	}
}

void releaseKey1(int key, int x, int y){
	switch (key) {
			case GLUT_KEY_UP :
			case GLUT_KEY_DOWN : cardy = 0;break;
		}
}



void processNormalKeys(unsigned char key, int x, int y) {
	
	if(key == 'r')
	{

	}

	if (key == 's')
		{
			if(filtercard==0)
			{
				carddisplayflag=1;
				//cout << carddisplayflag << endl;
			}
			if(filtercard==1)
			{
				carddisplayflag=0;
			}
		}
	

}



void releaseKey(unsigned char key, int x, int y) {

	if (key == 's')
	{
		filtercard=!filtercard;
	}
	if(key == 'r')
	{
		
		diceflag=0;
		xrotation=0;
		yrotation=0;
		zrotation=0;
	}
}


void reshape_main(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}

void displaymain()
{	
	
	glutSetWindow(mainwindow);
	glClearColor(0.9, 0.8, 0.8, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glutPostRedisplay();
   	glutSwapBuffers();
}

int main(int argc,char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_DEPTH | GLUT_RGBA | GLUT_ALPHA);
	glutInitWindowPosition(10,10);
	glutInitWindowSize(1200,700);
	

	mainwindow = glutCreateWindow("Monopoly");
	glutDisplayFunc(displaymain);
	//glutIgnoreKeyRepeat(1);
	//glutKeyboardFunc(processNormalKeys);
	//glutKeyboardUpFunc(releaseKey);
	//glutReshapeFunc(reshape_main);

	gamewindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(reshape_game);
	glutDisplayFunc(displaygame);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(releaseKey);
	initial_game();
	
	dicewindow = glutCreateSubWindow(mainwindow,900,0,150,150);
	glutReshapeFunc(reshape_dice);
	glutDisplayFunc(displaysdice);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(releaseKey);
	
	initial_dice();

	moneywindow = glutCreateSubWindow(mainwindow,1050,0,150,150);
	glutReshapeFunc(reshape_money);
	glutDisplayFunc(display_money);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(releaseKey);
	initial_money();
	
	cardwindow = glutCreateSubWindow(mainwindow,0,0,900,700);
	glutReshapeFunc(reshape_card);
	glutDisplayFunc(display_card);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);

	initial_card();

	glutMainLoop();
	return 0;
}
