/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef SPHERE_H_
#define SPHERE_H_

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "imageloader.h" // For rendering the bmp files
#include <cstdlib>
#include <ctime>
#include "render.h"

#define PI 3.141592655

 
using namespace std;
extern int currentdisplayflag;
extern GLuint globewindow,globemainwindow;
extern GLuint mainwindow,gamewindow,dicewindow,moneywindow,cardwindow,infowindow,playerwindow;   //SubWindows 
extern GLuint textureId_space;
extern GLuint selectedsnowman;
extern int mode;
extern float globerotation;
extern bool startleftrotation;
extern bool startrightrotation;

extern float globeupdownrotation;
extern bool startuprotation;
extern bool startdownrotation;
extern bool hasgamestarted;

void handleKeypress(unsigned char key, int x, int y);
void generaterandompoints();
void init_globe();
void processHits2 (GLint hits, GLuint buffer[], int sw);
void startPicking();
void stopPicking();
void resize_globe(int w, int h);
void render_globe();
void mouseStuff(int button, int state, int x, int y);
void update(int value);

#endif