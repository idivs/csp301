/*
	GameInit.cpp 
	Created by :
	Divyanshu Rathi
	5/09/2014
*/

#ifndef __GAMEINIT_H_INCLUDED__  
#define __GAMEINIT_H_INCLUDED__

#include <iostream>
#include <string>
#include <vector>
#include "math.h"
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
using namespace std;


extern bool isturncompleted[5];
extern int whichplayerturn;
extern int startlocationno;
extern int kplane;
extern float jplane;
extern bool showplanetransition;
extern int playerwhocanbuy;
extern int whichplayeristhis;

class City
{
public:
	string* name;
	int group;
	int cost;
	int mortagecost;
	int house1cost;
	int house2cost;
	int house3cost;
	int house4cost;
	int hotelcost;
	int rent0;
	int rent1;
	int rent2;
	int rent3;
	int rent4;
	int renthotel;
	int isbought;
	int boughtby;
	int totalhouses;
	int ishotel;
};

class Game
{
public:
	string* currency;
	int startingmoney;
	int jailfine;
	int tax;
	vector <City> cities;
	int map[50][50];
	int totalcolorgroups;
	float colorcodes[20][3];
	int whichplayerturn=1;
	bool isturncompleted[5]={1,1,1,1,1};
	void Generatecolorgroups();

	string *name[5];
	float hismoney[5];
	int citiesowned[5][100][5];
	int noofcitiesowned[5];
	int citiesofeachgroup[5][50];
	int currentlocation[5];
	int temporarylocation[5];
};



#endif