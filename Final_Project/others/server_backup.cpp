#include "Message.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/asio.hpp>
#include <iostream>


using namespace std;
int main()
{
    bool issend=0;
    Message msg;
    Message msg1;

    msg1._a="hi";
    msg1._b="accept";
    // deserialize


     boost::asio::io_service io_service;
            const uint16_t port = 1234;
            boost::asio::ip::tcp::acceptor acceptor(
                    io_service,
                    boost::asio::ip::tcp::endpoint(
                        boost::asio::ip::address::from_string("10.0.0.8" ),
                        port
                        )
                    );

    for(; ;)
    {   
            boost::asio::ip::tcp::socket socket( io_service );
            acceptor.accept( socket );
            std::cout << "connection from " << socket.remote_endpoint() << std::endl;

            // read header
            size_t header;
            boost::asio::read(
                    socket,
                    boost::asio::buffer( &header, sizeof(header) )
                    );
            std::cout << "body is " << header << " bytes" << std::endl;

            // read body
            boost::asio::streambuf buf;
            const size_t rc = boost::asio::read(
                    socket,
                    buf.prepare( header )
                    );


            buf.commit( header );

            std::cout << "read " << rc << " bytes" << std::endl;
            std::istream is( &buf );
            boost::archive::text_iarchive ar( is );
            
            
            ar & msg;

            std::cout << msg._a << std::endl;
            std::cout << msg._b << std::endl;
            
            

            while(true)
            {

                getline(cin,msg1._a);
                getline(cin,msg1._b);

                boost::asio::streambuf buf1;
                std::ostream os1( &buf1 );
                boost::archive::text_oarchive ar1( os1);
                ar1 & msg1;

                const size_t header1 = buf1.size();
                std::cout << "buffer size " << header1 << " bytes" << std::endl;
                
                // send header and buffer using scatter
                std::vector<boost::asio::const_buffer> buffers1;

                buffers1.push_back( boost::asio::buffer(&header1, sizeof(header1)) );
                buffers1.push_back( buf1.data() );



                const size_t rc1 = boost::asio::write(
                        socket,
                        buffers1
                        );
                std::cout << "wrote " << rc1 << " bytes" << std::endl;
            
            
            }
            
        }

}
    