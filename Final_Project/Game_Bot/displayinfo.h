/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef DISPLAYINFO_H_INCLUDED
#define DISPLAYINFO_H_INCLUDED

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>

void displaymouseclicklocation();

#endif
