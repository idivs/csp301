/*
	runserver.h
	Created by :
	Divyanshu Rathi
	5/09/2014
*/

#ifndef __RUNSERVER_H_INCLUDED__  
#define __RUNSERVER_H_INCLUDED__

#include "GameInit.h"
#include "runserver.h"
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/asio.hpp>


void runserver();
#endif