 /*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

#include "render.h"
#include "airport.h"

#define PI 3.141592655
using namespace std;


GLuint textureId[5];


/*Function to load textures */
void gametexture(void)
{

	Image* image7 = loadBMP("./images/grass.bmp");
	textureId[0] = loadTexture2(image7);
	delete image7;

	Image* image8 = loadBMP("./images/road.bmp");
	textureId[1] = loadTexture2(image8);
	delete image8;

	Image* image9 = loadBMP("./objects/plane/Texture_0.bmp");
	textureId[2] = loadTexture2(image9);
	delete image9;

	Image* image10 = loadBMP("./images/sky12.bmp");
	textureId[3] = loadTexture2(image10);
	delete image10;

	Image* image11 = loadBMP("./images/surface.bmp");
	textureId[4] = loadTexture2(image11);
	delete image11;


}

/*Render Grass */
void rendergrass(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-100.5,0.0,-100.5);
      glTexCoord2f(0.0f, 8.0f);glVertex3f(-100.5,0.0,100.5);
      glTexCoord2f(8.0f, 8.0f);glVertex3f(100.5,0.0,100.5);
      glTexCoord2f(8.0f, 0.0f);glVertex3f(100.5,0.0,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/* Render Surface */
void rendersurface(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[4]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-100.5,0.0,-20.5);
      glTexCoord2f(0.0f, 1.0f);glVertex3f(-100.5,0.0,20.5);
      glTexCoord2f(1.0f, 1.0f);glVertex3f(100.5,0.0,20.5);
      glTexCoord2f(1.0f, 0.0f);glVertex3f(100.5,0.0,-20.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/*Render Sky */
void rendersky(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[3]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-30.5,-30.0,-20.5);
      glTexCoord2f(0.0f, 1.0f);glVertex3f(-30.5,30.0,-20.5);
      glTexCoord2f(1.0f, 1.0f);glVertex3f(30.5,30.0,-20.5);
      glTexCoord2f(1.0f, 0.0f);glVertex3f(30.5,-30.0,-20.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}


/* Render Road */
void renderroad(void)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[1]);
	glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 0.0f);glVertex3f(-2.5,0.05,-100.5);
      glTexCoord2f(0.0f, 1.0f);glVertex3f(-2.5,0.05,2.5);
      glTexCoord2f(1.0f, 1.0f);glVertex3f(2.5,0.05,2.5);
      glTexCoord2f(1.0f, 0.0f);glVertex3f(2.5,0.05,-100.5);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/* Render Car */
void rendercar(GLMmodel *myModel) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glmFacetNormals(myModel);
	glmVertexNormals(myModel, 1000);
	glmDraw(myModel,GLM_TEXTURE | GLM_MATERIAL |  GLM_SMOOTH);
	glDisable(GL_TEXTURE_2D);
}
