/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef DICE_H_INCLUDED
#define DICE_H_INCLUDED

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"

extern int facevalue;;
extern int diceflag;
extern bool isdicecomplete;
extern GLfloat xrotation;
extern GLfloat yrotation;
extern GLfloat zrotation;
extern bool isdicerolled;
extern int remainingvalueofdice;

void setdicevalue();
void renderdice(void);
void dicetexture(void);

#endif
