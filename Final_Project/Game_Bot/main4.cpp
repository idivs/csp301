#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files

#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/thread.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/vector.hpp>

#include <boost/serialization/vector.hpp>

#include <boost/serialization/list.hpp>
#include <boost/serialization/string.hpp>
#include <boost/asio.hpp>
#include <fstream>
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "render.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "dice.h"
#include "money.h"
#include "sphere.h"
#include "airport.h"
#include "GameInit.h"
#include "displayinfo.h"
#include "Player.h"
#include "Gameplay.h"
#include "bot.h"
#include "runserver.h"

using namespace std;
#define PI 3.141592655

extern int finalparser();
GLMmodel *mymodel; //to load Model




	GLuint texture1;


float playerj=0.0;

int w;
int h;
float cardy=0;
float playery=0;
int carddisplayflag =0;
int playerdisplayflag=0;
int lastdisplayflag=0;

bool filtercard=0;
bool filterdice=0;
bool filterplayer=0;
bool filtergame=0;
bool filterglobe=1;


GLuint DLid1;
Game Monopoly;


 //    const uint16_t port = 1234;
 //    boost::asio::io_service io_service;
	// boost::asio::ip::tcp::acceptor acceptor1(
 //            io_service,
 //            boost::asio::ip::tcp::endpoint(
 //                boost::asio::ip::address::from_string("10.0.0.8" ),
 //                port
 //                )
 //            );
	// boost::asio::ip::tcp::socket socket1( io_service );
	
// void runserver123()
// {
// 	std::cout << "connection from " << socket1.remote_endpoint() << std::endl;
    
//     	boost::asio::streambuf buf;
//         std::ostream os( &buf );
//         boost::archive::text_oarchive ar( os );
//         ar & Monopoly;

//         const size_t header = buf.size();
//         std::cout << "buffer size " << header << " bytes" << std::endl;
        
//         // send header and buffer using scatter
//         std::vector<boost::asio::const_buffer> buffers;

//         buffers.push_back( boost::asio::buffer(&header, sizeof(header)) );
//         buffers.push_back( buf.data() );

//         const size_t rc = boost::asio::write(
//                 socket1,
//                 buffers
//                 );
//         std::cout << "wrote " << rc << " bytes" << std::endl;

        
//         // read header
//         size_t header1;
//         boost::asio::read(
//                 socket1,
//                 boost::asio::buffer( &header1, sizeof(header1) )
//                 );
//         std::cout << "body is " << header1 << " bytes" << std::endl;

//         // read body
//         boost::asio::streambuf buf1;
//         const size_t rc1 = boost::asio::read(
//                 socket1,
//                 buf1.prepare( header1 )
//                 );


//         buf1.commit( header1 );
    

//         std::cout << "read " << rc1 << " bytes" << std::endl;
//         std::istream is1( &buf1 );
//         boost::archive::text_iarchive ar1( is1 );
        
        
//         ar1 & Monopoly;

//         std::cout << "Player Turn: " << Monopoly.whichplayerturn << std::endl; 
        
// }
    
/* Generates Random value for the dice using time seeding. */
void generatediceface()
{
	srand(time(NULL));
	facevalue = (rand() % 6) + 1;
	remainingvalueofdice=facevalue;
}

/* Creating display list for the game */
GLuint creategameDL() 
{
	GLuint planeDL;
	planeDL = glGenLists(1);
	glNewList(planeDL,GL_COMPILE);
	rendercar(mymodel);
	glEndList();
	return(planeDL);
}

/*Initialising the main game that is the Aeroplane and city */
void initial_game()
{
	glClearColor(0.0,0.0,0.0,0.3);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	mymodel = initObject("./objects/plane/plane.obj");
	gametexture();

	// citymodel = initObject("./objects/City4/t.obj");
	citymodel = initObject("./objects/City3/Cyprys_House.obj");


	

	DLid1 = creategameDL();
}

/* Reshaping Function */
void reshape_game (int width , int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,1000.0f);   	// Calculate The Aspect Ratio Of The Window
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-20.0f);				// Move Right 1.5 Units And Into The Screen 6.0
	glRotatef(10,1.0f,0.0f,0.0f);
}

/* Display function for the game */
void displaygame()
{
	if(showplanetransition==1)
	{

		if(currentdisplayflag==0)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==1)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==2)
		{

			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==3)
		{
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutShowWindow();
		}	
		glutSetWindow(gamewindow);
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
		glColor3f(1.0,1.0,1.0);
		glColor4f(1,1,1,0);
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-20.0f);				// Move Right 1.5 Units And Into The Screen 6.0
		glRotatef(10,1.0f,0.0f,0.0f);
		GLfloat sizeofspace=50;
		renderroad();
		rendergrass();
		//glLoadIdentity();
	    glPushMatrix();
	   	if(jplane<10)
	   	{
	   		kplane=kplane+1;
	   		jplane=(jplane+0.02);
	   		glTranslatef(-0.5, 0.30+jplane/5, -1.5-kplane*1.0f/8);
	   		glScalef(0.004,0.004,0.004);
	    	glCallList(DLid1);
	    	glutPostRedisplay();
	    	
	   	}
	   	else
	   	{

	   		Monopoly.currentlocation[Monopoly.whichplayerturn]=Monopoly.temporarylocation[Monopoly.whichplayerturn];

			playerwhocanbuy=Monopoly.whichplayerturn;
	   		showplanetransition=0;
	   		currentdisplayflag=0;
	   		if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].isbought==0)
	   		{
	   			locationbuytag=1;
	   		}
	   		else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel==0 && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby==playerwhocanbuy)
	   		{
	   			locationbuytag=1;
	   		}
	   		else if(Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].ishotel==1 && Monopoly.cities[Monopoly.currentlocation[Monopoly.whichplayerturn]].boughtby==playerwhocanbuy)
	   		{
				isdicerolled=0;
				currentdisplayflag=3;
				locationbuytag=0;
				userwantstobuy=0;
				Monopoly.whichplayerturn=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
	   		}
	   		else
	   		{
	   			payrent();	   		}

	   		}
	   	glPopMatrix();
	   	glutSwapBuffers();
	}
	else
	{
		if(currentdisplayflag==0)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==1)
		{

			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(cardwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==2)
		{

			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutShowWindow();
		}
		else if(currentdisplayflag==3)
		{
			glutSetWindow(cardwindow);
			glutHideWindow();
			glutSetWindow(gamewindow);
			glutHideWindow();
			glutSetWindow(playerwindow);
			glutHideWindow();
			glutSetWindow(globemainwindow);
			glutShowWindow();
		}
		//cout << "Player has been transported to : " << Monopoly.currentlocation << endl;/
		//cout << locationbuytag << endl;
		
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		

		glutSetWindow(gamewindow);
		// glDisable(GL_LIGHTING);
		// glDisable(GL_LIGHT0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clearing thE Color Buffer
		glColor3f(1.0,1.0,1.0);
		glColor4f(1,1,1,0);
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-20.0f);				// Move Right 1.5 Units And Into The Screen 6.0
		glRotatef(10,1.0f,0.0f,0.0f);
		GLfloat sizeofspace=50;
		//renderroad();
		//rendergrass();
		rendersurface();
		rendersky();
		

	// 	GLfloat light_position[]={5,5,-10,0};
	// GLfloat white[]={1,1,1,1};
	// glMaterialfv(GL_FRONT,GL_SPECULAR,white);
	
	// glLightfv(GL_LIGHT0,GL_POSITION,light_position);
	// glEnable(GL_LIGHTING);
	// glEnable(GL_LIGHT0);
	// glEnable(GL_COLOR_MATERIAL);

  		
		glLoadIdentity();
		// GLfloat sizeofspace123 = 5;
		// glEnable(GL_TEXTURE_2D);
		// glBindTexture(GL_TEXTURE_2D,texture1);
		// glBegin(GL_QUADS);
  //   // Front Face
		//     glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofspace123, -sizeofspace123,  -10.0f);  // Bottom Left Of The Texture and Quad
		//     glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofspace123, -sizeofspace123,  -10.0f);  // Bottom Right Of The Texture and Quad
		//     glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofspace123,  sizeofspace123,  -10.0f);  // Top Right Of The Texture and Quad
		//     glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofspace123,  sizeofspace123,  -10.0f);  // Top Left Of The Texture and Quad
  // 		glEnd();
  // 		glDisable(GL_TEXTURE_2D);
		//glScalef(0.01,0.01,0.01);
		//glTranslatef(0,-10,-100);
		/*
		glTranslatef(0.0f,0.0f,10.0f);				// Move Right 1.5 Units And Into The Screen 6.0
		glRotatef(-10,1.0f,0.0f,0.0f);
		// /glScalef(0.01,0.01,0.01);

		
		
		glColor3f(1.0,1.0,1.0);
		
		*/
		//glTranslatef(100.0f,0.0f,-599.0f);
		glTranslatef(-0.8,0.0f,-20.0f);
		loadcityingame();

		if(locationbuytag==1)
		{	
			//cout << locationbuytag << endl;
			if(userwantstobuy==1)
			{
				//cout <<"bye bye bye " << endl;
				buylocation();
				//DLplayer = createplayerDL();
				locationbuytag=0;
				userwantstobuy=0;
				isdicerolled=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
				currentdisplayflag=3;
				if(Monopoly.whichplayerturn==1)
				{
					Monopoly.whichplayerturn=0;
					// boost::thread t(&runserver123);
				}
				else
				{
					Monopoly.whichplayerturn=1;
				}
				
			}

			else if(userwantstobuy==-1)
			{
				isdicerolled=0;
				currentdisplayflag=3;
				locationbuytag=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
				userwantstobuy=0;
				if(Monopoly.whichplayerturn==1)
				{
					Monopoly.whichplayerturn=0;
					// boost::thread t(&runserver123);
					
				}
				else
				{
					Monopoly.whichplayerturn=1;
				}
				
			}

			else if(Monopoly.whichplayerturn==0)
			{
				buylocation();
				cout << "Player 0 bought location :" << Monopoly.cities[Monopoly.temporarylocation[0]].name << " " << Monopoly.temporarylocation[0] << endl;
				isdicerolled=0;
				currentdisplayflag=3;
				locationbuytag=0;
				Monopoly.isturncompleted[Monopoly.whichplayerturn]=1;
				userwantstobuy=0;
				if(Monopoly.whichplayerturn==1)
				{
					Monopoly.whichplayerturn=0;
					// boost::thread t(&runserver123);
					
				}
				else
				{
					Monopoly.whichplayerturn=1;
				}
			}
		}
		//cout << "hi1235555" << endl;
		glutPostRedisplay();	
		glutSwapBuffers();
	}

    
}

/* Rendering dice */
void initial_dice()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	// glClearColor(0.5, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);

}
void reshape_dice (int width, int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
}

/* Displaying the dice */
void displaysdice()
{

	if(Monopoly.isturncompleted[Monopoly.whichplayerturn]==0 && diceflag==0 && isdicerolled==0)
	{
		generatediceface();
		isdicerolled=1;
	    cout << facevalue << endl;
	}
	
	else if(Monopoly.whichplayerturn==0 && isdicerolled==0)
	{
		diceflag=0;
		Monopoly.isturncompleted[Monopoly.whichplayerturn]=0;
		xrotation=0;
		yrotation=0;
		zrotation=0;
		generatediceface();
		isdicerolled=1;
	    cout << facevalue << endl;

	    findnodesatdistancek(Monopoly.currentlocation[0],facevalue);
	    int whichtobuy=mainaiforbot();

	    Monopoly.temporarylocation[0]=whichtobuy;
	    int endlocationofplayer=Monopoly.temporarylocation[Monopoly.whichplayerturn];
        currentdisplayflag=0;
        showplanetransition=1;
        kplane=0;
        jplane=0;
	}
	//else if()
	// if(diceflag==0)
	// {
	// 	if(isdicerolled==0)
	// 	{
	// 		generatediceface();
	// 		cout << facevalue << endl;
	// 		isdicerolled=1;
	// 	}
	// }
		glutSetWindow(dicewindow);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices									// Select The Modelview Matrix
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glRotatef(xrotation,0.5,0.0,0.0);		// Rotation about x-axis
		glRotatef(yrotation,0.0,0.5,0.0);       // Rotation about y-axis
		glRotatef(zrotation,0.0,0.0,0.5);		// Rotation about z-axis
		// if(diceflag==0)
		// {
		// 	if(isdicerolled==0)
		// 	{
		// 		generatediceface();
		// 		cout << facevalue << endl;
		// 		isdicerolled=1;
		// 	}
		// }
		// if(hasgamestarted==0 && diceflag==0)
		// {
		// 	generatediceface();
		// 	cout << facevalue << endl;
		// 	hasgamestarted=1;
		// 	isdicerolled=1;	
		// }
		//glRotatef(90,0.5,0.0,0.0);	
		//glEnable( GL_CULL_FACE );
	    //glCullFace ( GL_BACK );
	    setdicevalue();
		renderdice();
		glutPostRedisplay();
		glutSwapBuffers();
}

/* All the functions for the card */
void reshape_card(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}
void initial_card()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	DLcard = createcardDL();
	
}

/* Displaying the card */
void display_card()
{

	if(currentdisplayflag==0)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==1)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==2)
	{

		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==3)
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutShowWindow();
	}
	// if(carddisplayflag==1)
	// {
	// 	glutSetWindow(gamewindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutplayerWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutShowWindow();
	// }
	/*
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	*/
	// if(playerdisplayflag==1)
	// {
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutShowWindow();
	// }
	// else
	// {
	// 	glutSetWindow(playerwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(gamewindow);
	// 	glutShowWindow();
	// }
	
	glutSetWindow(cardwindow);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glMatrixMode(GL_PROJECTION);
	gluLookAt( 0,cardy,0,
	0, cardy,-1 ,
	0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glCallList(DLcard);
	glutPostRedisplay();
   	glutSwapBuffers();
}

/* Reshaping the player sub-window */
void reshape_player(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}
void initial_player()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//dicetexture();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	//DLplayer = createplayerDL();
	
}

/*This is to display the location card sub-window */
void display_player()
{
	if(currentdisplayflag==0)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==1)
	{

		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(cardwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==2)
	{

		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutShowWindow();
	}
	else if(currentdisplayflag==3)
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutHideWindow();
		glutSetWindow(playerwindow);
		glutHideWindow();
		glutSetWindow(globemainwindow);
		glutShowWindow();
	}
	// if(carddisplayflag==1)
	// {
	// 	glutSetWindow(gamewindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutplayerWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutShowWindow();
	// }
	/*
	else
	{
		glutSetWindow(cardwindow);
		glutHideWindow();
		glutSetWindow(gamewindow);
		glutShowWindow();
	}
	*/
	// if(playerdisplayflag==1)
	// {
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(cardwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(playerwindow);
	// 	glutShowWindow();
	// }
	// else
	// {
	// 	glutSetWindow(playerwindow);
	// 	glutHideWindow();
	// 	glutSetWindow(gamewindow);
	// 	glutShowWindow();
	// }
	
	glutSetWindow(playerwindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	glMatrixMode(GL_PROJECTION);
	gluLookAt( 0,cardy,0,
	0, cardy,-1 ,
	0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	
	//DLplayer = createplayerDL();
	//glCallList(DLplayer);
	renderplayercard2();
	glutPostRedisplay();
   	glutSwapBuffers();
}

/* Money Sub-Window */
void reshape_money(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	//glFrustum(-2.0, 2.0, -2.5, 2.5, 1, 40);														// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	// Move Right 1.5 Units And Into The Screen 6.0
	gluLookAt(0.0f, 0.0f, 5.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);												// Select The Modelview Matrix
	glLoadIdentity();
}

void initial_money()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	dicetexture();
	// glClearColor(0.5, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);

}

void display_money()
{
	glutSetWindow(moneywindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	//glPushMatrix();										// Select The Modelview Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	displaycurrentamount();
	glutPostRedisplay();
	glutSwapBuffers();
}


/* Display the info in the right bottom corner */
void resize_info(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height*0.8,1.0f,100.0f); 

	gluLookAt( 0,0,0.0,
	0, 0,-1 ,
	0.0f, 1.0f, 0.0f);  	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	glTranslatef(0,2,0);
	glScalef(1.5,1.5,1);
}

void init_info()
{
	glEnable(GL_DEPTH_TEST);				// Enable Depth 
	glShadeModel(GL_FLAT);
	//glClearColor(0.2, 0.8, 0.8, 0.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void render_info()
{
	glutSetWindow(infowindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);					// Color to be used to draw Vertices
	

	glMatrixMode(GL_MODELVIEW);

	if(selectedsnowman!=0)
	{
	  //cout << "hi";
	  displaymouseclicklocation();
	}
	if(selectedsnowman==0)
	{ 
		glColor3f(1,1,1);
		char startgamedisplay[]="STARTPOINT --> You get Fixed $$ everytime you arrive here";
		renderBitmapString(-1.6f, 0.55f, -9.989f, (void *)font ,startgamedisplay);
	}
	glutPostRedisplay();
	glutSwapBuffers();
}


/* These all are the Keyboard Functions */
void processSpecialKeys(int key, int xx, int yy) 
{

	float fraction = 0.4f;
	float fraction1 = 0.05f;
	float fraction2=1.0f;
	if(currentdisplayflag==1)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
				cardy=cardy+fraction;
				break;
			case GLUT_KEY_DOWN :
				cardy=cardy-fraction;
				break;
		}
	}
	else if(currentdisplayflag==2)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
				playery=playery+fraction1;
				break;
			case GLUT_KEY_DOWN :
				playery=playery-fraction1;
				break;
		}
	}
	else
	{
		switch(key)
		{
			case GLUT_KEY_LEFT :
				startleftrotation=1;
				break;
			case GLUT_KEY_RIGHT :
				startrightrotation=1;
				break;
			case GLUT_KEY_UP :
				startuprotation=1;
				break;
			case GLUT_KEY_DOWN :
				startdownrotation=1;
				break;
		}
	}
}
	

void releaseKey1(int key, int x, int y){
	if(currentdisplayflag==1)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
			case GLUT_KEY_DOWN : cardy = 0;break;
		}
	}
	else if(currentdisplayflag==2)
	{
		switch (key) 
		{
			case GLUT_KEY_UP :
			case GLUT_KEY_DOWN : playery = 0;break;
		}
	}
	else
	{	
		switch (key) 
		{
			case GLUT_KEY_LEFT :
				startleftrotation=0;
				break;
			case GLUT_KEY_RIGHT :
				startrightrotation=0;
				break;
			case GLUT_KEY_UP :
				startuprotation=0;
				break;
			case GLUT_KEY_DOWN :
				startdownrotation=0;
				break;
		}
	}
}

void processNormalKeys(unsigned char key, int x, int y) {
	
	if(key == 'r')
	{

	}
	else if(key=='b')
	{

	}

	else if (key == 'g')
	{
		if(filtergame==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=0;
			//cout << carddisplayflag << endl;
		}
		if(filtergame==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}

	else if (key == 's')
	{
		if(filtercard==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=1;
			//cout << carddisplayflag << endl;
		}
		if(filtercard==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}
	else if (key == 'p')
	{

		if(filterplayer==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=2;

			//cout << carddisplayflag << endl;
		}
		if(filterplayer==1)
		{
			currentdisplayflag=lastdisplayflag;
		}
	}
	else if (key == 'l')
	{
		if(filterglobe==0)
		{
			lastdisplayflag=currentdisplayflag;
			currentdisplayflag=3;
			//cout << carddisplayflag << endl;
		}
		if(filterglobe==1)
		{
			//currentdisplayflag=lastdisplayflag;
			currentdisplayflag=0;
		}
	}
}


void releaseKey(unsigned char key, int x, int y) 
{

	if (key == 'g')
	{
		filtergame=!filtergame;
	}
	if (key == 's')
	{
		filtercard=!filtercard;
	}
	if (key == 'p')
	{
		filterplayer=!filterplayer;
	}
	if (key == 'l')
	{
		filterglobe=!filterglobe;
	}
	if(key == 'r')
	{
		if(Monopoly.whichplayerturn==whichplayeristhis)
		{
			hasgamestarted=1;
			diceflag=0;
			Monopoly.isturncompleted[Monopoly.whichplayerturn]=0;
			xrotation=0;
			yrotation=0;
			zrotation=0;
		}
	}
	if(key=='y')
	{
		if(playerwhocanbuy==Monopoly.whichplayerturn)
		{
			userwantstobuy=1;
		}
	}
	if(key=='n')
	{
		if(playerwhocanbuy==Monopoly.whichplayerturn)
		{
			userwantstobuy=-1;
		}
	}
}


/* Main Background window to accomodate all the sub-windows */
void reshape_main(int width,int height)
{
	glMatrixMode(GL_PROJECTION);											// Select The Projection Matrix
	glViewport(0,0,width,height);											// Reset The Current Viewport
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);   	// Calculate The Aspect Ratio Of The Window
	//gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}

void displaymain()
{	
	

	glutSetWindow(mainwindow);
	// glClearColor(0.9, 0.8, 0.8, 0.0);
	glClearColor(0, 0, 0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glutPostRedisplay();
   	glutSwapBuffers();
}

/* Main Function that does all the OpenGL window initalisation */
int main(int argc,char** argv)
{

	// acceptor1.accept( socket1 );
	// std::cout << "connection from " << socket1.remote_endpoint() << std::endl;
	

	cout <<	finalparser() << endl;  //Parsing Function
	Monopoly.Generatecolorgroups(); //Generating Random Color Groups
	srand (time(NULL)); //Random Time Seeding
	generaterandompoints(); //Generating Random points
	initialiseplayer(); //Initialising the player dat
	findnodesatdistancek(0,3); // Finding nodes from node a  using c steps. (a,c)

	glutInit(&argc,argv); //Glut Initialise
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_DEPTH | GLUT_RGBA); //Setting the buffers available.
	glutInitWindowPosition(10,10); //Position of the window
	glutInitWindowSize(1200,700); // Size of the window
	
	mainwindow = glutCreateWindow("Monopoly"); //Title of the Main window
	glutDisplayFunc(displaymain); 
	glutMouseFunc(mouseStuff);
		//glutReshapeFunc(reshape_main);

	globewindow = glutCreateSubWindow(mainwindow,900,120,300,250); //Sub-Window Named Globe
	glutReshapeFunc(resize_globe);
	glutDisplayFunc(render_globe);
	glutMouseFunc(mouseStuff);
	//glutTimerFunc(25,update,0);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_globe();

	globemainwindow = glutCreateSubWindow(mainwindow,0,0,900,700); //Globe main sub window
	glutReshapeFunc(resize_globe);
	glutDisplayFunc(render_globe);
	glutMouseFunc(mouseStuff);
	//glutTimerFunc(25,update,0);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_globe();


	infowindow = glutCreateSubWindow(mainwindow,900,370,300,400); //Info subwindow
	glutReshapeFunc(resize_info);
	glutDisplayFunc(render_info);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	init_info();

	gamewindow = glutCreateSubWindow(mainwindow,0,0,900,700); //Flights and Cities Sub-window
	glutReshapeFunc(reshape_game);
	glutDisplayFunc(displaygame);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	
	initial_game();
	 
	dicewindow = glutCreateSubWindow(mainwindow,900,0,150,120); // Dice Sub-window
	glutReshapeFunc(reshape_dice);
	glutDisplayFunc(displaysdice);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);	//glutReshapeFunc(reshape_main);
	
	
	initial_dice();

	moneywindow = glutCreateSubWindow(mainwindow,1050,0,150,120); // Money Sub-window
	glutReshapeFunc(reshape_money);
	glutDisplayFunc(display_money);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(releaseKey);
	
	initial_money();
	
	cardwindow = glutCreateSubWindow(mainwindow,0,0,900,700); // Card Sub-window
	glutReshapeFunc(reshape_card);
	glutDisplayFunc(display_card);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);
	initial_card();

	playerwindow = glutCreateSubWindow(mainwindow,0,0,900,700); //Player data sub-window
	glutReshapeFunc(reshape_player);
	glutDisplayFunc(display_player);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(releaseKey1);
	glutKeyboardUpFunc(releaseKey);
	initial_player();


	glutMainLoop(); // Infinite loop for the OpenGL
	return 0;
}
