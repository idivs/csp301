var searchData=
[
  ['pathname',['pathname',['../struct__GLMmodel.html#a06f09c6f9c93cec84232e848a0ce1b2b',1,'_GLMmodel']]],
  ['pixels',['pixels',['../classImage.html#a6afbcf4b0a2774f020ce350bff9d0d6c',1,'Image']]],
  ['playerdisplayflag',['playerdisplayflag',['../main4_8cpp.html#a47efe532dfb8e17160e1f6a6c9c096ea',1,'main4.cpp']]],
  ['playerj',['playerj',['../main4_8cpp.html#a6dcbbc4ea6f407ee9f813af7b67f34c7',1,'main4.cpp']]],
  ['playerwhocanbuy',['playerwhocanbuy',['../GameInit_8cpp.html#ad87db67a9bcebf7903623ea0274e6fb3',1,'playerwhocanbuy():&#160;GameInit.cpp'],['../GameInit_8h.html#ad87db67a9bcebf7903623ea0274e6fb3',1,'playerwhocanbuy():&#160;GameInit.cpp']]],
  ['playerwindow',['playerwindow',['../sphere_8cpp.html#ac450f06e4c4ec4178987792d2f4c3243',1,'playerwindow():&#160;sphere.cpp'],['../sphere_8h.html#ac450f06e4c4ec4178987792d2f4c3243',1,'playerwindow():&#160;sphere.cpp']]],
  ['playery',['playery',['../main4_8cpp.html#a7ca2d91c5cd63453a28148d791fad4be',1,'main4.cpp']]],
  ['polygons',['polygons',['../struct__GLMmodel.html#a83c9670d29b8c91c71aeb71eb0bc6753',1,'_GLMmodel']]],
  ['position',['position',['../struct__GLMmodel.html#acab837a5075b2f0301b4c6c8c4a465b4',1,'_GLMmodel']]]
];
