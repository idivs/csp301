/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef RENDER_H_
#define RENDER_H_

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>

#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>

#define PI 3.141592655
using namespace std;

//extern GLuint textureId[9];


GLMmodel* initObject(const char* filename);
GLuint loadTexture2(Image* image);



#endif
