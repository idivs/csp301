
/*
 * Divyanshu Rathi
 *
 *  Created on: 28-October-2013
 *      Author: Divyanshu Rathi
 */
#ifndef AIRPORT_H_
#define AIRPORT_H_

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>

#include <string.h>
#include <stdio.h>
#include <float.h>

#include <cstdlib>
#include <ctime>
#include "render.h"

void gametexture(void);
void rendergrass(void);
void renderroad(void);
void rendercar(GLMmodel *myModel);
void rendersky();
void rendersurface();

#endif
