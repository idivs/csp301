#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include "imageloader.h" // For rendering the bmp files
#include <iostream>
#include "math.h"
#include <vector>
#include "glm.h"
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"
#include "money.h"
#include "displayinfo.h"
#include "sphere.h"
#include "GameInit.h"

//char mouselocationname[50];
extern Game Monopoly;

using namespace std;



char locationboughtyes[]="Sold to Player : ";
char noofhouseconstructed[]="No.of Houses Constructed:";
char ishotelcons[]="Is Hotel Constructed : ";
char yeschar[]="Yes";
char nochar[]="No";
char locationboughtno[]="Available for Buying";
char locationcostdisplay[]="Cost:";
char locationcost[30];



void displaymouseclicklocation()
{
	/*
	//cout << "hi" << selectedsnowman;
	//snprintf(displaymoney, "%f", usermoney);
	sprintf(locationcost,"%d",Monopoly.cities[selectedsnowman].cost);
	renderBitmapString(-2.0f, 1.2f, 0.0f, (void *)font2 ,(char*)Monopoly.cities[selectedsnowman].name->c_str());
	renderBitmapString(-2.0f, 0.8f, 0.0f, (void *)font1 ,locationcostdisplay);
	renderBitmapString(-1.0f, 0.8f, 0.0f, (void *)font1 ,locationcost);
	//renderBitmapString(-1.0f, 0.5f, 0.0f, (void *)font ,dollarsymbol);
	//renderBitmapString(-0.8f, 0.5f, 0.0f, (void *)font ,displaymoney);
	*/

	char properycost[30];
		sprintf(properycost,"%d",Monopoly.cities[selectedsnowman].cost);
		char costwithh1[10];
		sprintf(costwithh1,"%d",Monopoly.cities[selectedsnowman].house1cost);
		char costwithh2[10];
		sprintf(costwithh2,"%d",Monopoly.cities[selectedsnowman].house2cost);
		char costwithh3[10];
		sprintf(costwithh3,"%d",Monopoly.cities[selectedsnowman].house3cost);
		char costwithh4[10];
		sprintf(costwithh4,"%d",Monopoly.cities[selectedsnowman].house4cost);
		char costwithhotel[10];
		sprintf(costwithhotel,"%d",Monopoly.cities[selectedsnowman].hotelcost);
		char mortage[10];
		sprintf(mortage,"%d",Monopoly.cities[selectedsnowman].mortagecost);
		char rent0[10];
		sprintf(rent0,"%d",Monopoly.cities[selectedsnowman].rent0);
		char rent1[10];
		sprintf(rent1,"%d",Monopoly.cities[selectedsnowman].rent1);
		char rent2[10];
		sprintf(rent2,"%d",Monopoly.cities[selectedsnowman].rent2);
		char rent3[10];
		sprintf(rent3,"%d",Monopoly.cities[selectedsnowman].rent3);
		char rent4[10];
		sprintf(rent4,"%d",Monopoly.cities[selectedsnowman].rent4);
		char renthotel[10];
		sprintf(renthotel,"%d",Monopoly.cities[selectedsnowman].renthotel);

		//glEnable(GL_TEXTURE_2D);
		//glBindTexture(GL_TEXTURE_2D,textureId[0]);

		glColor3f(Monopoly.colorcodes[Monopoly.cities[selectedsnowman].group-1][0],Monopoly.colorcodes[Monopoly.cities[selectedsnowman].group-1][1],Monopoly.colorcodes[Monopoly.cities[selectedsnowman].group-1][2]);
		glBegin(GL_QUADS);
	    glTexCoord2f(0.0f, 0.0f); glVertex3f(-breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 0.0f); glVertex3f( breadthofcard, -lengthofcard-0.9,  -10);  
	    glTexCoord2f(1.0f, 1.0f); glVertex3f( breadthofcard,  lengthofcard,  -10);  
	    glTexCoord2f(0.0f, 1.0f); glVertex3f(-breadthofcard,  lengthofcard,  -10); 
	   	glEnd();
	   	//glDisable(GL_TEXTURE_2D);

	   	glColor3f(1,1,1);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder-0.9,  -9.999);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.999);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.999); 
	    
	   	glEnd();
	   	glColor3f(0.984,0.8352,0.611);
	   	glBegin(GL_QUADS);
	    glVertex3f(-breadthofcard+cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder, -lengthofcard+cardborder+1.8,  -9.990);  
	    glVertex3f( breadthofcard-cardborder,  lengthofcard-cardborder,  -9.990);  
	    glVertex3f(-breadthofcard+cardborder,  lengthofcard-cardborder,  -9.990); 
	   	glEnd();

	   	glColor3f(0.0,0.0,0.0);
	   	
	   	renderBitmapString(-0.7f, 0.8f, -9.989f, (void *)font1 ,(char*)Monopoly.cities[selectedsnowman].name.c_str());

	   	renderBitmapString(-0.7f, 0.55f, -9.989f, (void *)font ,ccost);
	   	renderBitmapString(0.4f, 0.55f, -9.989f, (void *)font ,properycost);

	   	renderBitmapString(-0.7f, 0.35f, -9.989f, (void *)font ,ch1);
	   	renderBitmapString(0.4f, 0.35f, -9.989f, (void *)font ,costwithh1);

	   	renderBitmapString(-0.7f, 0.15f, -9.989f, (void *)font ,ch2);
	   	renderBitmapString(0.4f, 0.15f, -9.989f, (void *)font ,costwithh2);

	   	renderBitmapString(-0.7f, -0.05f, -9.989f, (void *)font ,ch3);
	   	renderBitmapString(0.4f, -0.05f, -9.989f, (void *)font ,costwithh3);

	   	renderBitmapString(-0.7f, -0.25f, -9.989f, (void *)font ,ch4);
	   	renderBitmapString(0.4f, -0.25f, -9.989f, (void *)font ,costwithh4);

	   	renderBitmapString(-0.7f, -0.45f, -9.989f, (void *)font ,chotel);
	   	renderBitmapString(0.4f, -0.45f, -9.989f, (void *)font ,costwithhotel);

	   	renderBitmapString(-0.7f, -0.65f, -9.989f, (void *)font ,constantrent);
	   	renderBitmapString(0.4f, -0.65f, -9.989f, (void *)font ,rent0);

	 
	   	renderBitmapString(-0.7f, -0.85f, -9.989f, (void *)font ,crent1);
	   	renderBitmapString(0.4f, -0.85f, -9.989f, (void *)font ,rent1);

	   	renderBitmapString(-0.7f, -1.05f, -9.989f, (void *)font ,crent2);
	   	renderBitmapString(0.4f, -1.05f, -9.989f, (void *)font ,rent2);

	   	renderBitmapString(-0.7f, -1.25f, -9.989f, (void *)font ,crent3);
	   	renderBitmapString(0.4f, -1.25f, -9.989f, (void *)font ,rent3);

	   	renderBitmapString(-0.7f, -1.45f, -9.989f, (void *)font ,crent4);
	   	renderBitmapString(0.4f, -1.45f, -9.989f, (void *)font ,rent4);

	   	renderBitmapString(-0.7f, -1.65f, -9.989f, (void *)font ,chostel1);
	   	renderBitmapString(0.4f, -1.65f, -9.989f, (void *)font ,renthotel);

	   	renderBitmapString(-0.7f, -1.85f, -9.989f, (void *)font ,cmort);
	   	renderBitmapString(0.4f, -1.85f, -9.989f, (void *)font ,mortage);

	   	glColor3f(1,1,1);
	   	if(Monopoly.cities[selectedsnowman].isbought==1)
	   	{
	   			char soldtolocation[10];
	   		char noofhousesint[10];
	   		int testvar=1;
	   		renderBitmapString(-0.9f, -2.45f, -9.989f, (void *)font1 ,locationboughtyes);
	   		sprintf(soldtolocation,"%d",Monopoly.cities[selectedsnowman].boughtby);
	   		renderBitmapString(0.6f, -2.45f, -9.989f, (void *)font1 ,soldtolocation);


	   		renderBitmapString(-0.9f, -2.65f, -9.989f, (void *)font1,noofhouseconstructed);
	   		sprintf(noofhousesint,"%d",Monopoly.cities[selectedsnowman].totalhouses);
	   		renderBitmapString(0.8f, -2.65f, -9.989f, (void *)font1 ,noofhousesint);

	   		renderBitmapString(-0.9f, -2.85f, -9.989f, (void *)font1,ishotelcons);
	   		if(Monopoly.cities[selectedsnowman].ishotel==1)
	   		{
	   			renderBitmapString(0.8f, -2.85f, -9.989f, (void *)font1 ,yeschar);
	   		}
	   		else
	   		{
	   			renderBitmapString(0.8f, -2.85f, -9.989f, (void *)font1 ,nochar);
	   		}
	   	}
	   	else
	   	{
	   		
	   		renderBitmapString(-0.9f, -2.45f, -9.989f, (void *)font2 ,locationboughtno);
	   	

	   	}
}


