#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include <iostream>
#include "math.h"
#include "glm.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "imageloader.h" // For rendering the bmp files
#include <cstdlib>
#include <ctime>
#include "textdisplay.h"
#include "locationcard.h"
#include "render.h"
#include "money.h"
#include "displayinfo.h"
#include "sphere.h"
#include "GameInit.h"
#include "Player.h"

using namespace std;

#define PI 3.14159265
#define RENDER          1
#define SELECT          2
#define SELECT1         3
#define BUFSIZE 1024

GLuint globewindow,globemainwindow;
GLuint mainwindow,gamewindow,dicewindow,moneywindow,cardwindow,infowindow,playerwindow;   //SubWindows 
int currentdisplayflag=3;
GLuint selectBuf[BUFSIZE];
GLint hits;
int mode = RENDER;
int cursorX,cursorY;
GLuint selectedsnowman=0;
float vertices123[40][3];
GLuint textureId_globe; //The id of the globe texture
GLuint textureId_space;
GLUquadric *quad;
//float rotate;
extern Game Monopoly;
float globerotation;
float globeupdownrotation=0;
bool startuprotation=0;
bool startdownrotation=0;
bool startleftrotation=0;
bool startrightrotation=0;
bool hasgamestarted=1;

//GLUquadricObj quad
void handleKeypress(unsigned char key, int x, int y) 
{
  switch (key) 
  {
    case 27: //Escape key
    exit(0);
  }
}

// Picking Stuff //

void generaterandompoints()
{
  for(int i=0;i<Monopoly.cities.size();i++)
  {
   float theta = 2*PI * (rand()%1000)/1000.0 ;//random range is 0.0 to 1.0
   float phi = acos(2.0f * (rand()%1000)/1000.0 - 1.0f)*180/(PI);
   float rho = 3;
    float y=rho*cos(phi*PI/180);
    float z=-(rho*sin(phi*PI/180)*cos(theta));
    float x=rho*sin(phi*PI/180)*sin(theta);
  	vertices123[i][0]=x;
  	vertices123[i][1]=y;
  	vertices123[i][2]=z;
  	cout << phi << endl;
  	//cout << x << " " << y << " " << z << endl;
  	//cout << (rand()%1000)/1000.0 << endl;
  	//cout << rho << " " << theta << " " << phi << endl;
  }
}

//bezier curve
GLfloat bezierCurve(float t, GLfloat P0,
                    GLfloat Monopoly, GLfloat P2) {
    // Cubic bezier Curve
    GLfloat point = pow(t,2)*P0 + 2*Monopoly*t*(1-t) + P2*pow(1-t, 2);
    return point;
}



void init_globe() 
{
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_LIGHTING);
  //glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  quad = gluNewQuadric();
  Image* image = loadBMP("./images/venus.bmp");
  textureId_globe = loadTexture2(image);
  delete image;

  Image* image1= loadBMP("./images/space.bmp");
  textureId_space=loadTexture2(image1);
  delete image1;
}

void processHits2 (GLint hits, GLuint buffer[], int sw)
{

   GLint i, j, numberOfNames;
   GLuint names, *ptr, minZ,*ptrNames;

   ptr = (GLuint *) buffer;
   minZ = 0xffffffff;
   for (i = 0; i < hits; i++) {	
      names = *ptr;
	  ptr++;
	  if (*ptr < minZ) {
		  numberOfNames = names;
		  minZ = *ptr;
		  ptrNames = ptr+2;
	  }
	  
	  ptr += names+2;
	}
   if (numberOfNames > 0) {
    
	  printf ("You picked Location: ");

	  ptr = ptrNames;
	  for (j = 0; j < numberOfNames; j++,ptr++) {
      if(mode==SELECT1)
      {
        if(Monopoly.map[Monopoly.temporarylocation[Monopoly.whichplayerturn]][*ptr]==1)
        {
          remainingvalueofdice=remainingvalueofdice-1;
          Monopoly.temporarylocation[Monopoly.whichplayerturn]=*ptr;
          if(remainingvalueofdice==0)
          {
            int endlocationofplayer=Monopoly.temporarylocation[Monopoly.whichplayerturn];
            currentdisplayflag=0;
            showplanetransition=1;
            kplane=0;
            jplane=0;
          }
        }
      }
      selectedsnowman=*ptr;

      //cout << selectedsnowman;
		 printf ("%d ", *ptr);
	  }
	}
   else
	   printf("You didn't pick a location!");
  printf ("\n");
   
}
void startPicking() 
{
	GLint viewport[4];
	float ratio;
	glSelectBuffer(BUFSIZE,selectBuf);
	glGetIntegerv(GL_VIEWPORT,viewport);
	glRenderMode(GL_SELECT);
	glInitNames();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPickMatrix(cursorX,viewport[3]-cursorY,5,5,viewport);
	ratio = (viewport[2]+0.0) / viewport[3];
	gluPerspective(45,ratio,0.1,1000);
	glMatrixMode(GL_MODELVIEW);
}
void stopPicking() {

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glFlush();
	hits = glRenderMode(GL_RENDER);
	if (hits != 0){
		processHits2(hits,selectBuf,0);

	}
	mode = RENDER;
}

void resize_globe(int w, int h) {
  //cout << h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

}

void render_globe() 
{

  char startgamedisplay[]="Welcome to the 3D Monopoly";
  char startgamedisplay1[]="Press r to roll the dice and then select the location";
  GLfloat sizeofspace =40;
 // cout << currentdisplayflag << endl;
  if(currentdisplayflag==3)
  {
    glutSetWindow(gamewindow);
    glutHideWindow();
    glutSetWindow(cardwindow);
    glutHideWindow();
    glutSetWindow(playerwindow);
    glutHideWindow();
    glutSetWindow(globewindow);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glColor4f(1,1,1,1);
    if(hasgamestarted==0)
    {
      renderBitmapString(-1.8f, 0.55f, -9.989f, (void *)font ,startgamedisplay);
      renderBitmapString(-3.5f, 0.15f, -9.989f, (void *)font ,startgamedisplay1);
    }
    else if(Monopoly.whichplayerturn!=0 && Monopoly.isturncompleted[Monopoly.whichplayerturn]==0)
    {
      char message1[50]="No. of Moves Remaining:";
      char movevalue[5];
      sprintf(movevalue,"%d",remainingvalueofdice);
      renderBitmapString(-0.5f, 0.55f, -9.989f, (void *)font1 ,message1);
      renderBitmapString(0.8f, 0.15f, -9.989f, (void *)font1 ,movevalue);
    }
    else if(Monopoly.whichplayerturn==0)
    {
      char displaycomp[]="Computer's Turn";
      renderBitmapString(-0.5f, 0.55f, -9.989f, (void *)font1 ,displaycomp);
    }
    else if(Monopoly.whichplayerturn!=0 && Monopoly.whichplayerturn!=whichplayeristhis)
    {
      char displaycomp[]="Current Player Playing : ";
      char playerdisplayturn[20];
      sprintf(playerdisplayturn,"%d",Monopoly.whichplayerturn);
      renderBitmapString(-0.5f, 0.55f, -9.989f, (void *)font1 ,displaycomp);
      renderBitmapString(0.8f, 0.15f, -9.989f, (void *)font1 ,playerdisplayturn);
    }
    else if(Monopoly.whichplayerturn==whichplayeristhis)
    {
      char displaycomp[]="Your Turn Now";
      //char playerdisplayturn[20];
      //sprintf(playerdisplayturn,"%d",Monopoly.whichplayerturn);
      renderBitmapString(-0.5f, 0.55f, -9.989f, (void *)font1 ,displaycomp);
      //renderBitmapString(0.8f, 0.15f, -9.989f, (void *)font1 ,playerdisplayturn);
    }
    glutSwapBuffers();
    glutSetWindow(globemainwindow);
    glutShowWindow();
    
    //cout << "hithere";
  }
  else if(currentdisplayflag==0)
  {

    glutSetWindow(playerwindow);
    glutHideWindow();
    glutSetWindow(cardwindow);
    glutHideWindow();
    glutSetWindow(globemainwindow);
    glutHideWindow();
    glutSetWindow(gamewindow);
    glutShowWindow();
    glutSetWindow(globewindow);
    glutShowWindow();
  }
  else if(currentdisplayflag==1)
  {

    glutSetWindow(playerwindow);
    glutHideWindow();
    glutSetWindow(globemainwindow);
    glutHideWindow();
    glutSetWindow(gamewindow);
    glutHideWindow();
    glutSetWindow(cardwindow);
    glutShowWindow();
    glutSetWindow(globewindow);
    glutShowWindow();
  }
  else if(currentdisplayflag==2)
  {

    glutSetWindow(cardwindow);
    glutHideWindow();
    glutSetWindow(globemainwindow);
    glutHideWindow();
    glutSetWindow(gamewindow);
    glutHideWindow();
    glutSetWindow(playerwindow);
    glutShowWindow();
    glutSetWindow(globewindow);
    glutShowWindow();
  }
 // else{
 //      glutSetWindow(globemainwindow);
 //      glutHideWindow();
 //      glutSetWindow(gamewindow);
 //      glutShowWindow();
 //      glutSetWindow(globewindow);
 //      glutShowWindow();
 // }
  //glClearColor(0.5, 0.8, 0.8, 0.0);
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);  
  glLoadIdentity();
  glColor4f(1,1,1,0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, textureId_space);
  glBegin(GL_QUADS);
    // Front Face
    glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofspace, -sizeofspace,  -50.0f);  // Bottom Left Of The Texture and Quad
    glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofspace, -sizeofspace,  -50.0f);  // Bottom Right Of The Texture and Quad
    glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofspace,  sizeofspace,  -50.0f);  // Top Right Of The Texture and Quad
    glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofspace,  sizeofspace,  -50.0f);  // Top Left Of The Texture and Quad
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glTranslatef(0.0f, 0.0f, -15.0f);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, textureId_globe);
  //Bottom
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  //glRotatef(rotate,1.0f,0.0f,0.0f);
  if(startleftrotation!=0)
  {
    if(globerotation==360)
    {
      globerotation=0;
    }
    globerotation=globerotation+1;
    

  }
  if(startrightrotation!=0)
  {
    if(globerotation==-360)
    {
      globerotation=0;
    }
    globerotation=globerotation-1;
   
  }
  if(startuprotation!=0)
  {
    if(globeupdownrotation==360)
    {
      globeupdownrotation=0;
    }
    globeupdownrotation=globeupdownrotation+1;
    
  }
  if(startdownrotation!=0)
  {
    if(globeupdownrotation==360)
    {
      globeupdownrotation=0;
    }
    globeupdownrotation=globeupdownrotation-1;
    

  }
  glRotatef(globerotation,0.0f,1.0f,0.0f);
  
  
  glRotatef(globeupdownrotation,1.0f,0.0f,0.0f);


     GLfloat startx=vertices123[selectedsnowman][0];
      GLfloat starty=vertices123[selectedsnowman][1];
      GLfloat startz=vertices123[selectedsnowman][2];
      
      //glColor3f(0.0, 0.0, 0.0);

      for(int j1=0;j1<Monopoly.cities.size()-1;j1++)
        {
          if(Monopoly.map[selectedsnowman][j1]==1)
          {
            GLfloat endx=vertices123[j1][0];
            GLfloat endy=vertices123[j1][1];
            GLfloat endz=vertices123[j1][2];

            GLfloat px=(startx+endx)/2;
            GLfloat py=(starty+endy)/2;
            GLfloat pz=(startz+endz)/2;

            //vector3f tan1(perpBisectorDirection.x/10*15,perpBisectorDirection.y/10*15,perpBisectorDirection.z/10*15);
            
            glLineWidth(2.0);
            glBegin(GL_LINE_STRIP);
            int t = 100;
            for (int i = 1; i <= t; i++) 
            {
                float pos = (float) i / (float) t;
                GLfloat x = bezierCurve( pos,startx, px/3*30, endx);
                GLfloat y = bezierCurve( pos,starty, py/3*30, endy);
                // In our case, the z should always be empty
                GLfloat z = bezierCurve(pos,startz, pz/3*30, endz);
                glVertex3f(x, y, z);
            }
            glEnd();
          }

        }
    
    
/*
    for (int i1=0; i1<Monopoly.cities.size()-1; i1++)
    {
      GLfloat startx=vertices123[i1][0];
      GLfloat starty=vertices123[i1][1];
      GLfloat startz=vertices123[i1][2];


        if(Monopoly.temporarylocation[Monopoly.whichplayerturn]==i1)
            {
              glLineWidth(4.0);
              glColor3f(0.0, 0.0, 0.0);
            }
            else
            {
              glLineWidth(2.0);
              //glColor3f(1.0, 0.0, 0.0);
              glColor3f(Monopoly.colorcodes[Monopoly.cities[i1].group-1][0],Monopoly.colorcodes[Monopoly.cities[i1].group-1][1],Monopoly.colorcodes[Monopoly.cities[i1].group-1][2]);

            }

        for(int j1=0;j1<Monopoly.cities.size()-1;j1++)
        {
          if(Monopoly.map[i1][j1]==1)
          {
            GLfloat endx=vertices123[j1][0];
            GLfloat endy=vertices123[j1][1];
            GLfloat endz=vertices123[j1][2];

            GLfloat px=(startx+endx)/2;
            GLfloat py=(starty+endy)/2;
            GLfloat pz=(startz+endz)/2;

            //vector3f tan1(perpBisectorDirection.x/10*15,perpBisectorDirection.y/10*15,perpBisectorDirection.z/10*15);
            
            
            glBegin(GL_LINE_STRIP);
            int t = 100;
            for (int i = 1; i <= t; i++) 
            {
                float pos = (float) i / (float) t;
                GLfloat x = bezierCurve( pos,startx, px/3*50, endx);
                GLfloat y = bezierCurve( pos,starty, py/3*50, endy);
                // In our case, the z should always be empty
                GLfloat z = bezierCurve(pos,startz, pz/3*50, endz);
                glVertex3f(x, y, z);
            }
            glEnd();
          }

        }
    }
    */
    glColor4f(1.0, 1.0, 1.0,1.0);
    gluQuadricTexture(quad,1);
    gluSphere(quad,2.6,20,20);
    glDisable(GL_TEXTURE_2D);
	
	if (mode == SELECT || mode == SELECT1) {
    cout << "Mode is:"<< mode << endl;
		startPicking();
	}
	//glLoadIdentity();
  //cout << Monopoly.cities.size() << endl;
  GLfloat sizeofspace1=0.3;
	for(int kj=0;kj<Monopoly.cities.size();kj++)
	{
		glPushMatrix();
		glPushName(kj);
		glTranslatef(vertices123[kj][0],vertices123[kj][1],vertices123[kj][2]);
    //renderBitmapString(0.0f, 0.0f,0.0f, (void *)font1 ,(char*)Monopoly.cities[kj].name->c_str());
    glColor3f(Monopoly.colorcodes[Monopoly.cities[kj].group-1][0],Monopoly.colorcodes[Monopoly.cities[kj].group-1][1],Monopoly.colorcodes[Monopoly.cities[kj].group-1][2]);

		//glColor3f(kj*1.0f/12,kj*1.0f/12,kj*1.0f/12);
		//gluSphere(quad,0.3,20,20);

		glutWireSphere(0.3,20,20);
    if(Monopoly.temporarylocation[Monopoly.whichplayerturn]==kj)
    {
      glColor4f(1,1,1,0);
      glBindTexture(GL_TEXTURE_2D, textureId_globe);
      glBegin(GL_QUADS);
      // Front Face
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofspace1, -sizeofspace1,  0.0f);  // Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofspace1, -sizeofspace1,  0.0f);  // Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofspace1,  sizeofspace1,  0.0f);  // Top Right Of The Texture and Quad
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofspace1,  sizeofspace1,  0.0f);  // Top Left Of The Texture and Quad
    glEnd();
    glDisable(GL_TEXTURE_2D);
    }
     if(Monopoly.currentlocation[1]==kj)
    {
       glColor4f(1,0,1,0);
      glBindTexture(GL_TEXTURE_2D, textureId_globe);
      glBegin(GL_QUADS);
      // Front Face
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-sizeofspace1, -sizeofspace1,  0.0f);  // Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0f, 0.0f); glVertex3f( sizeofspace1, -sizeofspace1,  0.0f);  // Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0f, 1.0f); glVertex3f( sizeofspace1,  sizeofspace1,  0.0f);  // Top Right Of The Texture and Quad
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-sizeofspace1,  sizeofspace1,  0.0f);  // Top Left Of The Texture and Quad
      glEnd();
      glDisable(GL_TEXTURE_2D);
    }
		glPopName();
		glPopMatrix();

	}

  

      

  
  /*
  if(selectedsnowman!=0)
  {
    //glLoadIdentity();
    //glTranslatef(0.0f, 1.0f, -16.0f);
    glPushMatrix();
    glTranslatef(vertices123[selectedsnowman][0],vertices123[selectedsnowman][1],vertices123[selectedsnowman][2]);
    gluSphere(quad,1,20,20);
    glPopMatrix();
  }
  */
  glutPostRedisplay();
	//cout << theta << " " << phi << " " << rho << endl;
  if (mode == SELECT || mode == SELECT1)
		stopPicking();
	else
		glutSwapBuffers();

}

void mouseStuff(int button, int state, int x, int y) 
{
	
  if(button==GLUT_LEFT_BUTTON && state != GLUT_DOWN)
  {
    cursorX = x;
    cursorY = y; 
    mode = SELECT;
  }

  else if(button==GLUT_RIGHT_BUTTON && state != GLUT_DOWN && Monopoly.whichplayerturn!=0 && remainingvalueofdice!=0)
  {
    cursorX = x;
    cursorY = y; 
    mode = SELECT1;
  }
  //cout << mode << endl;
}
/*
void processMousePassiveMotion(int x, int y) 
{
  cursorX = x;
  cursorY = y;
  mode = SELECT;
}
*/

/*
void update(int value)
{
    globerotation+=1.0f;
    if(globerotation>360.f)
    {
        globerotation-=360;
    }
    
    glutTimerFunc(25,update,0);
}
*/


/*
int main(int argc, char** argv) {	
 
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(800, 800);
  mainwindow = glutCreateWindow("Monopoly");
  gamewindow = glutCreateSubWindow(mainwindow,0,0,600,800);
  initRendering();
  glutTimerFunc(25,update,0);
  glutDisplayFunc(drawScene);
  glutKeyboardFunc(handleKeypress);
  glutReshapeFunc(handleResize);
  glutMouseFunc(mouseStuff);
  //glutPassiveMotionFunc(processMousePassiveMotion);

  dicewindow = glutCreateSubWindow(mainwindow,600,0,200,800);
  glutReshapeFunc(reshape_dice);
  glutDisplayFunc(displaysdice);
  glutMainLoop();
  return 0;
*/


