/*
	GameInit.cpp 
	Created by :
	Divyanshu Rathi
	5/09/2014
*/

#include <iostream>
#include <string>
#include <vector>
#include "GameInit.h"
#include "math.h"
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
using namespace std;

extern Game Monopoly;
int startlocationno=0;

bool isturncompleted[5]={1,1,1,1,1};
// int whichplayerturn=1;
int kplane=1;
bool showplanetransition=0;
int playerwhocanbuy;

int whichplayeristhis=2;


float jplane=0.0;
void Game::Generatecolorgroups()
{
	srand (time(NULL));
	for(int i=0;i<totalcolorgroups;i++)
	{
		colorcodes[i][0]=1.0f*(rand()%10)/10;
		colorcodes[i][1]=1.0f*(rand()%10)/10;
		colorcodes[i][2]=1.0f*(rand()%10)/10;
	}

	for(int i=0;i<Monopoly.cities.size();i++)
	{
		Monopoly.cities[i].isbought=0;
		Monopoly.cities[i].boughtby=-1;
		Monopoly.cities[i].totalhouses=0;
		Monopoly.cities[i].ishotel=0;

	}
}
