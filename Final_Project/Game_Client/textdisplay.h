/*
	Created By : Divyanshu Rathi
	Date : 1st Nov 2014
*/
#ifndef TEXTDISPLAY_H_INCLUDED
#define TEXTDISPLAY_H_INCLUDED

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <OpenGL/OpenGL.h>
# include <GLUT/GLUT.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>
#endif
#include <iostream>
#include "math.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
extern void *font;
extern void *font1;
extern void *font2;
	void renderBitmapString(
		float x,
		float y,
		float z,
		void *font,
		char *string);
#endif
